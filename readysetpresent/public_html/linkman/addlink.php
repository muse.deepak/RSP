<?php
/*******************************************************************************
*  Title: LinkMan reciprocal link manager
*  Version: 1.5 @ May 22, 2008
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2004-2008 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden. Using this code, in part or full,
*  to create competing scripts or products is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
require('settings.php');
ini_set('user_agent', 'LinkMan '.$settings['verzija'].' by http://www.phpjunkyard.com');

/* Accepting any more links? */
$lines = @file($settings['linkfile']);
if (count($lines)>$settings['max_links'])
{
    problem('We are not accepting any more links at the moment. We appologize for the inconvenience!');
}

/* Check user input */
$name  = pj_input($_POST['name'],'Please enter your name!');
$email = pj_input($_POST['email'],'Please enter your e-mail address!');
if (!preg_match('/([\w\-]+\@[\w\-]+\.[\w\-]+)/',$email))
{
    problem('Please enter a valid e-mail address!');
}
$title = pj_input($_POST['title'],'Please enter the title (name) of your website!');
if (strlen($title)>50)
{
    problem('Title is too long! Limit website title to 50 chars!');
}

$url   = pj_input($_POST['url'],'Please enter the URL of your website!');
if (!(preg_match('/(http:\/\/+[\w\-]+\.[\w\-]+)/i',$url)))
{
    problem('Please enter valid URL of your website!');
}

$recurl = pj_input($_POST['recurl'],'Please enter the url where a reciprocal link to our site is placed!');
if (!(preg_match('/(http:\/\/+[\w\-]+\.[\w\-]+)/i',$recurl)))
{
    problem('Please enter valid URL of the page where the reciprocal link to our site is placed!');
}

/* Compare URL and Reciprocal page URL */
$parsed_url = parse_url($url);
$parsed_rec = parse_url($recurl);
if ($parsed_url['host'] != $parsed_rec['host'])
{
    problem('The reciprocal link must be placed under the same (sub)domain as your link is!');
}

$url    = str_replace('&amp;','&',$url);
$recurl = str_replace('&amp;','&',$recurl);

$description = pj_input($_POST['description'],'Please write a short description of your website!');
if (strlen($description)>200)
{
    problem('Description is too long! Description of your website is limited to 200 chars!');
}

/* Check if the website is banned */
if ($mydata = file_get_contents($settings['banfile']))
{
	/* Check website URL */
	$regex = str_replace(array('http://www.','http://'),'http://(www\.)?',$url);
	$regex = preg_replace('/index\.[^\/]+$/','',$regex);
	$regex = str_replace('/','\/',rtrim($regex,'/'));
	$regex = '/'.$regex.'\/?(index\.[^\/]+)?\s/i';
	if (preg_match($regex,$mydata))
	{
		problem('This website has been permanently banned from our link exchange!');
	}

	/* Check reciprocal link URL */
	$regex = str_replace(array('http://www.','http://'),'http://(www\.)?',$recurl);
	$regex = preg_replace('/index\.[^\/]+$/','',$regex);
	$regex = str_replace('/','\/',rtrim($regex,'/'));
	$regex = '/'.$regex.'\/?(index\.[^\/]+)?\s/i';
	if (preg_match($regex,$mydata))
	{
		problem('This website has been permanently banned from our link exchange!');
	}

	unset($mydata);
}

/* Check title and description for possible spam problems */
eval(gzinflate(base64_decode('FZfF0oVqskQfp88JBrjFHeEuG4dJB+7uPP39e86gqMovc2V5pc
M/9ddO1ZAe5T9ZupcE9t+izOei/Oc/fFKI0BysQu9GEECJDP22DtmSBjAxqEtaxHVVjC06Q+S/O7m+VA
KOBJAQn0Ku0/dl2q2TEdwHLjzR1EM9ZMnSHRk0ucDlzaR2xa2x8/Lmw4fnpeovsrRna5W8CPWcIDw5Rg
x0P0y7XVVsYlbrvB8ttPKoeH64dFSI8Zgax1M3D54zOze0bq1/64OXpdvBpbT644F2hfk9nKjcZ9taP5
Usu3fbm0uR3OJ6DnA7vo4aryg9UQzZ8HBXurllZPyJ492+4zHxoDMDDOUEeDyHMz9obO+aZ8dKZfpE8L
RQcolgoX4C7obcIBjDsZFLa404UWQm0R7VXeMujFZl28mJd6JNi032A8QdJNH18oR5IV4ILR8APfNixy
od1yrr8uqH5IyZsOEQxQFQiJeszBPuoE6NrePkdNPDyv5e8nZ97rU5rq9FK7S9zcpuk3mGTkffh9ZIWa
6fiT18nlylTdl8R8hzML7YckDmU8IBqgmaSTpJL4tisbEgBmHggWJuOO131q8R+eri2K9gR3xiQ5lvSw
0OLJx99omWXcU1EL8r2tiMQxOmc2+76ng9uxZZm3SPIHiG6Fm/35SwM0w8QYaOUAe6K6eUQTM7VN89QX
HJAHoiLJOzXCb/KORln1fcycGJwsE/IG9Hv4bwz24iC5NSLtrWtHQoSgQQdEyO/KpljCanhBBhSaA4pQ
t4FJfIRf7Hp0dKPKK1Gfp69Blwd0Z4HcGnpKVWEZvLtsO+cWmjYtG6MRAOnuyP7fgD7uCcYEUMh/2x8y
TUyHKo1lKxiFCv0XA5VW4xKNhZ03ofFTWZPwDnlYI5LJt1YZHh4PgK+TmqgE7AKJCbGQuA8PY+B6oLVU
MbSUQDrB9JVL+KZHWIFAc0SKnBtJcZTLtUckWuSRvBpAbiiPJIA8pqcSVFE501Yd245/7SuQU/PNgyMi
LUxft9PBWdjDIXEduJcm5brqMEYiRBXDXXab7bf4LdmZFeeXfqWDup4unxbvJPteNBWPbUK8gdwhhbZq
vPkqdQwHlo/pijja4/ybl2RnwzxFH4rBMWWERlqpjhB271T6kT0FssBkP8hSGir3FwJarK2eCVuLyZBe
vJ8aonK2y+qfRKuf5+5iyA2TOVj6NBRqLin7LQQSMDkOFfbUkfNYUGHxeegCdr7wTxEWbRVENzLZR1IS
mHaUB2gK9VwAp1QT5eExqMOaHq7ARxzdvIYIN3TFyvjPbdCJVTvAlpgGQbMwCEOVP/6YIVODpAhxK1ne
Wq8gBnkmazfLmj+zZ2QoxF6+PPbVbY/iLCKJGTzGCBH0KoYq11N6PeV8j4WDRNcaEOBsXri3romY3svK
DFHuSWHhYcVOE3Mjo0ezVa+3pgczP4bwUKxNOU7cOVY7MLS9tltySklSwJuMn0Zhb2IKbZkgb54DC+Jh
6Z8zs3nELG7v26IOV86d3udCdZF42XT2/J/c3Fq0yIrNpDo0YXBJVJQMpxsXsG29QNayzQfm6IgmCxDX
bJU8NXpaOIy3TXOGSc2L8zifpll0MHnQTbUY1zZ9GxTndn6Oq0lauWYz0K55favn/ckMUTZx6By/QIN8
IzeaDKfHNZiVimG9PtDVt6+4VwfX52JS00ya4jB3RMlc0aufe2H8SE3wLVa5k9BGgPUGMXazPP8adySY
tUkBuCBjKFmkAsWRyhGyJRkmb85NiQpBkU/syDud1WgzXNr2JO+Xung3tQst0N3lmprrLTNRgwRPBQxg
NUjY1rijAEtmzu9GTKIn/ljXqamb73hYwb8st3/byjvzvxN0ecWXS1ADqf5YRtCcg+KYCXz8z0tuqqeW
x31UMI7kfhrlOB0Nbf73AtJtRQPwBkMs85kLpPc38edTtRmdrA61tuHq6DEOEo3bIyZlwKOwXw9moutt
/uXR0OkQCgseI2euCsjFoHlmhYUcs4+GBY4PNQ10UcR6sugN9dOq8Jor5BX0SPKitklst5tQmthzC4qS
91FG9g0QKzr6nGTkwbkQp1mSrx/hRkP1uG6Y5klS9jCvURUxAIqnJ7yh5p05OVg7G0y70hoOP7EA9zIX
mbw4vfKBYYBRiR5OYlaR+++CPxdlc60W9eQBI23a/lfi0az0UE4cmgZfrRbGnhwjgrxVvB4AFAFsRFGB
cSbGcr2g3cL2rw32lePRqtaugJpR+5+AGFySUlslfcWZbRy7GTdhLQyKJk0bXnyIm3lejhVFY5aM5VPs
S0gQyIfGhUsx7wJHq0edns8TqyurUJDiz9YOJ1rQTHyQXyKIYY581f20PiOfy8y02ZLMgL+W4LMQxdJZ
uYyKmergwMUACKmrVSeWJUaF+TtLWewerwZqrzus7nVT/qEHH9iRZHinqyYa9+b1FxPu3Wr2WhdnzEMH
6YlSc6v6pUCCdIlaBe1YhDllKh1XIFTSgUPDtjysikbsSl2iwor03IkCA6ziM5RTrTPV4ZjRGM0c7liY
ViRbD9mMUfdSEv/d0pidhbW6TX9wLhNRGNNwE+2sVK4ouSAHtnLuaWj3fwBRjq9FwIFBMEts3mkbx0L9
g2Tt+TctnykJ+kGA31F2RVh1rbY+oVIumjF76+07dhAEIlffcS1AmOZUOi9UFFVlOSL0NsQyXdPbAMH6
Ut7Tf1NK3TGrk/MbsuF335rUElq0d8QYbK2dIHX0hjAI253IHxKIEg1QrhoahDjGj7MPDYM2Yf3MafXz
h5NWdBaSeWePGEwmaQCBcPyBREew15ippTu3aGZQmuHlU4xshswclYknJL3sw0G5xPyPVDzGQLVfqHLY
MIqbXrPIvzAhDwB5o6L+sAzaJOl3G2eYJ/ajGfoPnYbM+VX8XGfQHmLgLG9JMSJb4A5JVti/QXDZNJ3O
wmEQi/zgadeI0NvxUiPA4Q2/xBHm9hnKnChu6EACxrgskquodYNtjsXkFkN+ybLYnkWPRgRhu6vQzOdo
Hh4z8vCz4Gkw7ljJ4MK2heJmcR/MPMFyLzUmgg5NJSo6px6W6VidX9/ur9jXBlyZ7kVFwLnZckxMAWBA
EqlJWGVXfFmgrIhw8hhuwmM9toGZkPC96X+ViunfZ6aKl+EXjAHrlurhTNoclf2CPf+/M0zgLBm7SMhl
AjhRC1Og2HoxkA+HuE9qS8+Ggr9UrjtEiV9B4N03FVyCLlKotcb12dsdmQmhohzRp5S6diqT51wxBv41
lqGDMP36DJbQ690N+ujO4Avto1oHx+ztVFA3MhLD6OILOffKk5tieAjAPjEOjIRwxyKgBvWvovJPrDQO
tf+bf9jnObc8WPKHHFrhJ8cFU022zs8p3FQVI2+OraSJF6tJru4q6zkwlksTPbgV9//paw3KRmb+UWA6
fMO16hsvpnzNAv/wJRYgfvdWsXzG5ininck3AqeHobgVuo5Nkodxd3tw/Zk9JD+C4Pe2Lb8VL9weWnJ+
0sLBOJWzdV1W4bgvRlsCk91pTsWXJE3J3T39rfarcHODL4qap1mY66qYb433HV7HMuASxL5CfwCMnL7r
42dyvnRlcapYIKg6Q2bsaPRjR1GA9Lmi/T7R99atMR/aHADClAO9RK0HTASvz96LI3C+AWkrVQ0js20G
AsrmRCDQET8OQqxx5jjkoatWcirYTP6MUEByqsH1JrlCGRijngII0mKNDUspxfIVXTUuB4tSi4NJ9SVH
lEDZnaL6PwhXQJKAg5j509jFnnWSSgNsSwSiAx10JrKAqivWOvg33fmAZ0uP5Vde7JWFSDXdYGA/iqzZ
Pw/XmP5y++Ixs5vM94W8JDJaLATs4l1uEvVpIM5/nP2vIruIadofjFhmTNKTP8sr2O5bmIAN1t2qPf1g
epZcCKHm44UotICr/YRxMo9jtNqh84/kroUZFAQIekKPJl5uDZUx1i9Ijgvwes60hHXUmtKe1sU502Pg
WP7FEjjzMO/c7K07xcQkp/xfFB9qZi70xybVvi0RrmvvR+ph3H4/0IpbOoR2lM/uNZwN4B81OqV8x8rt
h43MdJNypDDw6NR7q2xqaduUr/YFQdDHbewC5fVUGp4JaXr3XUnt+fBZDo2YeoxS8PMeYnu7ZE81xpEf
ousQkndhUfCmUBjpRGmgx/ru/Jf8dXkMUaZPkT/toT5FgsDPicPavJ/NMB4PhfY2ObK0+ycVk+UBrNKw
RlDeXgeVdEIM/44J1CQHIqc1o0YaxVoSzMxdu/1/Pg8jEldI7QExULD7JYY81uhg3zXNVz+a4Cb9kXRm
QwIXhr/PrrrcL5QvnRERzsdsrRF4Qwk+OJRHWNMVuVRPmMbr9b4fplQaHjpOfOP/s+kV+b3zoQKDLYjd
Pgab4/DIcVUCdnregrUJfPvoj1WhV/JT8+QacTyqaiI82TzK6QJaxWLcmMT1yLDt6DV7itCoChvcHYKz
pGiPRoxrmKlR4lxMZdG9W2agquN52Wdat5jLMO1EG+2BsjUkLOCl0Vj1vFrzCUXYWSpM4dfCy8BzP2dF
Vxx/OXP37DJOut/zSH5hyc9+Msi0NW0ZprRCewaPBzTQCFQk96sIav2GTlOSf4r7q9fS6DvGxxF2kSVW
KkI4xnoFWqlwYSB6h1LHG5o8rfEz+VHG0caNAQ3nDsioSGJSBFZO/b+foDQqjVQh+Hiwx0qp39iQfBrh
y22L8noFULCZGKaP4qSFqq7fVlr0fdceaOJv6JRef7TZM/RtlZ2RIktFP9Ma4g59T3GI1yrHd7jo/ASz
we4ZAlItP2ohsOiu/RwLGHqzwmEL/j1BudvmOcEPKp8ztGqT/0+AgAlE4zRqBbCzj9QfifreB3uhivjL
W1PPNg5DQfaG+3NwPca5Zv/WMv6pM0FSdSmVXwHQn05sz+6r16q4EjnTwUzhek0PvINtijx+WfHtW/4Z
HABVC2rE8tFIizC1xMXsyuI+Gqbz5AT+QrbS71bx5Wt0my36ePBg+StFAgU4sEdWfIPFsyeAkHTCSQBV
C5vV+c5Rj1wmITolpKnBBloKrlQzZ9+ux+B/2ZTP++5smnMd4pO5o4AIBzrOIfmBd0KiwSMuMlmEUTAJ
RkAUKoH3N/HHF63v6+foFtCvlrTrJxaUOMg1MT9fD0RUYPqvYGXdxn3C/AD97+3tAWId2uEPm1qFn/C+
TR+THc4iuoj3+Rtb4P9HxrPKrwRAsr9DcVomVDaYjBooMfG/i2kJsgkoSIfWL2VUmnTUgVzMM25E5hr1
Tf75mD6n4xVq7eGjWfn+fiCpYaf0UvkUzATH6V36XMZEhH3DZobq2KhEVt9BlCSNwT9kYahTgUmhBye9
rCMd6eiDD7kV4ZILiRDO1XPpNcq4/r5pgI7qmgtjfrdWbSBw2ey4HyYYpNsFTeKbQZIuBzLn3UH90dIy
3xyS2ZNkMBP7J5t0zQQmI4h+xqfTH6uxf65+vXoKpWcRM5pAzwnZtNSxIoGC/KatEwy/iKLHUod8INXk
ucpJuV3vOJFVAseeRMT5/DnF876uXq8sEIwM2+CrL2IDxMiudqqIxK2nL5mmw1o2tPkIJAVHhOmQa3Dt
XqiMwjzgbMTOv58pEk4qFQYzAyKJZT1FSrFdbmQRpupcEF3LeEjbBuAd9mlF6cpi83IJB1wFnslwdXlZ
VXN9k4R4HcQ5M+SR4YP4aRRIDwz4z4iUv0WJcGCKdR637Drezr+B2Bb0bxwenlYjvCOV47yGpnPfxl+S
s+l86ShhOt3wb+phX8S6n6ZcKhpjTnvAoMqxuLlNUfb7uXIqCdqzBdCt8hd9sFaoMgTtM0CIIH+J9///
33//4f')));

if ($settings['spam_filter'])
{
    $test = pj_checkTitleDesc($title, $description, $url, $settings['superlatives']);
    if ($test === true)
    {
        $test = '';
    }
    elseif ($test == 'superlatives')
    {
        problem('Don\'t use superlatives (words like best, biggest, cheapest, largest) in title and description!');
    }
    elseif ($test == 'text')
    {
        problem('Your link failed SPAM test, we are forced to reject it.');
    }
    elseif ($test == 'url')
    {
        problem('Your link failed SPAM test, we are forced to reject it.');
    }
}

if ($settings['autosubmit'])
{
    session_start();
    if (empty($_SESSION['checked']))
    {
        $_SESSION['checked']  = 'N';
        $_SESSION['secnum']   = rand(10000,99999);
        $_SESSION['checksum'] = $_SESSION['secnum'].$settings['filter_sum'].date('dmy');
    }
    if ($_SESSION['checked'] == 'N')
    {
        print_secimg();
    }
    elseif ($_SESSION['checked'] == $settings['filter_sum'])
    {
        $_SESSION['checked'] = 'N';
        $secnumber = pj_isNumber($_POST['secnumber']);
        if(empty($secnumber))
        {
            print_secimg(1);
        }
        if (!check_secnum($secnumber,$_SESSION['checksum']))
        {
            print_secimg(2);
        }
    }
    else
    {
        problem('Internal script error. Wrong session parameters!');
    }
}

/* Check for duplicate links */
if ($settings['block_duplicates'])
{
    $mydata = file_get_contents($settings['linkfile']);

    /* Check website URL */
    $regex = str_replace(array('http://www.','http://'),'http://(www\.)?',$url);
    $regex = preg_replace('/index\.[^\/]+$/','',$regex);
    $regex = str_replace('/','\/',rtrim($regex,'/'));
    $regex = '/'.$regex.'\/?(index\.[^\/]+)?\s/i';
    if (preg_match($regex,$mydata))
    {
        problem('Please don\'t submit the same website more than once or we will be forced to delete all your links!');
    }

    /* Check reciprocal link URL */
    $regex = str_replace(array('http://www.','http://'),'http://(www\.)?',$recurl);
    $regex = preg_replace('/index\.[^\/]+$/','',$regex);
    $regex = str_replace('/','\/',rtrim($regex,'/'));
    $regex = '/'.$regex.'\/?(index\.[^\/]+)?\s/i';
    if (preg_match($regex,$mydata))
    {
        problem('Please don\'t submit multiple websites with the same reciprocal link URL or we will be forced to delete all your links!');
    }

    unset($mydata);
}

/* Get HTML code of the reciprocal link URL */
$html = @file_get_contents($recurl) or problem('Can\'t open remote URL!');
$html = strtolower($html);
$site_url = strtolower($settings['site_url']);

/* Block links with the meta "robots" noindex or nofollow tags? */
if ($settings['block_meta_rob']==1 && preg_match('/<meta([^>]+)(noindex|nofollow)(.*)>/siU',$html,$meta))
{
    problem(
        'Please don\'t place the reciprocal link to a page with the meta robots noindex or nofollow tag:<br />'.
        htmlspecialchars($meta[0])
    );
}

$found    = 0;
$nofollow = 0;

if (preg_match_all('/<a\s[^>]*href=([\"\']??)([^" >]*?)\\1([^>]*)>/siU', $html, $matches, PREG_SET_ORDER)) {
    foreach($matches as $match)
    {
        if ($match[2] == $settings['site_url'] || $match[2] == $settings['site_url'].'/')
        {
            $found = 1;
            if (strstr($match[3],'nofollow'))
            {
                $nofollow = 1;
            }
            break;
        }
    }
}

if ($found == 0)
{
    problem(
        'Our URL (<a href="'.$settings['site_url'].'">'.$settings['site_url'].
        '</a>) wasn\'t found on your reciprocal links page (<a href="'.$recurl.'">'.
        $recurl.'</a>)!<br><br>Please make sure you place this exact URL on your
        links page before submitting your link!'
    );
}

/* Block links with rel="nofollow" attribute? */
if ($settings['block_nofollow'] && $nofollow == 1)
{
    problem('Please don\'t use rel=&quot;nofollow&quot; link attribute for the reciprocal link!');
}

/* Check Google PageRank */
if ($settings['show_pr'] || $settings['min_pr'] || $settings['min_pr_rec'])
{
    require('pagerank.php');
    $pr = getpr($url);
    $pr = empty($pr) ? 0 : $pr;

    if ($settings['min_pr'] && ($pr < $settings['min_pr']))
    {
        problem('Unfortunately we accept only websites with Google PageRank '.$settings['min_pr'].'/10 or higher. Please try submitting your website again in a few months.');
    }

    if ($settings['min_pr_rec'])
    {
        $pr_rec = getpr($recurl);
        $pr_rec = empty($pr_rec) ? 0 : $pr_rec;
        if ($pr_rec < $settings['min_pr_rec'])
        {
            problem('Please place the reciprocal link to <a href="'.$settings['site_url'].'">'.$settings['site_url'].'</a> on a page with Google PageRank '.$settings['min_pr_rec'].'/10 or higher.');
        }
    }
}

$replacement = "$name$settings[delimiter]$email$settings[delimiter]$title$settings[delimiter]$url$settings[delimiter]$recurl$settings[delimiter]$description$settings[delimiter]0$settings[delimiter]$pr\n";

if ($settings['add_to'] == 0)
{
    /* Make sure new link is added after any featured ones */
	$i = 0;
	foreach ($lines as $thisline)
	{
		list($name2,$email2,$title2,$url2,$recurl2,$description2,$featured2,$pr2)=explode($settings['delimiter'],$thisline);
		$featured2 = $featured2 ? 1 : 0;
		if ($featured2 == 0)
		{
			$lines[$i] = $replacement . $thisline;
			break;
		}
		$i++;
	}

	$replacement = implode('',$lines);
    $fp = fopen($settings['linkfile'],'w') or problem('Couldn\'t open links file for writing! Please CHMOD all txt files to 666 (rw-rw-rw)!');
    flock($fp, LOCK_EX);
    fputs($fp,$replacement);
    flock($fp, LOCK_UN);
    fclose($fp);
}
else
{
    $fp = fopen($settings['linkfile'],'a') or problem('Couldn\'t open links file for appending! Please CHMOD all txt files to 666 (rw-rw-rw)!');
    flock($fp, LOCK_EX);
    fputs($fp,$replacement);
    flock($fp, LOCK_UN);
    fclose($fp);
}

if($settings['notify'] == 1)
{
$message = "Hello,

Someone just added a new link to your links page on $settings[site_url]

Link details:

Name: $name
E-mail: $email
URL: $url
Reciprocal link: $recurl
Title: $title
Description:
$description


End of message

";
    $headers  = "From: $name <$email>\n";
    $headers .= "Reply-To: $name <$email>\n";
    mail($settings['admin_email'],'New link submitted',$message,$headers);
}

require_once('header.txt');
?>
<p align="center"><b>Your link has been added!</b></p>
<p>&nbsp;</p>
<p align="center">Thank you, your link has been successfully added to our link exchange (try reloading our links page if you don't see your link there yet)!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p align="center"><a href="<?php echo $settings['site_url']; ?>">Back to the main page</a></p>
<?
require_once('footer.txt');
exit();

function problem($problem) {
require_once('header.txt');
echo '
    <p align="center"><font color="#FF0000"><b>ERROR</b></font></p>
    <p>&nbsp;</p>
    <p align="center">'.$problem.'</p>
    <p>&nbsp;</p>
    <p align="center"><a href="javascript:history.go(-1)">Back to the previous page</a></p>
';
require_once('footer.txt');
exit();
}

function print_secimg($message=0) {
global $settings;
$_SESSION['checked']=$settings['filter_sum'];
require_once('header.txt');
?>
<p>&nbsp;</p>

<p align="center"><b>Anti-SPAM check</b></p>

<div align="center"><center>
<table border="0">
<tr>
<td>
    <form action="addlink.php?<?php echo strip_tags(SID); ?>" method="POST" name="form">

    <hr>
    <?php
    if ($message == 1)
    {
        echo '<p align="center"><font color="#FF0000"><b>Please type in the security number</b></font></p>';
    }
    elseif ($message == 2)
    {
        echo '<p align="center"><font color="#FF0000"><b>Wrong security number. Please try again</b></font></p>';
    }
    ?>

    <p>This is a security check that prevents automated signups of this forum (SPAM).
    Please enter the security number displayed below into the input field and click the continue button.</p>

    <p>&nbsp;</p>

    <p>Security number: <b><?php echo $_SESSION['secnum']; ?></b><br>
    Please type in the security number displayed above:
    <input type="text" size="7" name="secnumber" maxlength="5"></p>

    <p>&nbsp;
    <?php
    foreach ($_POST as $k=>$v)
    {
        if ($k == 'secnumber')
        {
            continue;
        }
        echo '<input type="hidden" name="'.htmlspecialchars($k).'" value="'.htmlspecialchars(stripslashes($v)).'">';
    }
    ?>
    </p>

    <p align="center"><input type="submit" value=" Continue "></p>

    <hr>

    </form>
</td>
</tr>
</table>
</center></div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php
require_once('footer.txt');
exit();
}

function check_secnum($secnumber,$checksum) {
    global $settings;
    $secnumber .= $settings['filter_sum'].date('dmy');
    if ($secnumber == $checksum)
    {
        unset($_SESSION['checked']);
        return true;
    }
    else
    {
        return false;
    }
}
?>
