<?php

// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";
include('htmlMimeMail.php');
/*
function sendmail_attach($to, $subject, $text, $html, $attachments, $headers) {

	$OB="----=_OuterBoundary_000";
	$IB="----=_InnerBoundery_001";
	$headers .= "Content-Type: multipart/mixed;\n\tboundary=\"".$OB."\"\n";

	//Messages start with text/html alternatives in OB
	$msg = "This is a multi-part message in MIME format.\n";
	$msg .= "\n--".$OB."\n";
	$msg .= "Content-Type: multipart/alternative;\n\tboundary=\"".$IB."\"\n\n";

	//plaintext section 
	$msg .= "\n--".$IB."\n";
	$msg .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n";
	$msg .= "Content-Transfer-Encoding: quoted-printable\n\n";
	// plaintext goes here
	$msg.=$text."\n\n";

	// html section 
	$msg .= "\n--".$IB."\n";
	$msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
	$msg .= "Content-Transfer-Encoding: base64\n\n";
	// html goes here 
	$msg .= chunk_split(base64_encode($html))."\n\n";

	// end of IB
	$msg.="\n--".$IB."--\n";

	// attachments
	if ($attachments) {
		$attmFile = $attachments;
//		foreach ($attachments as $attmFile) {
			$patharray = explode ("/", $attmFile); 
			$fileName = $patharray[count($patharray)-1];
			$msg .= "\n--".$OB."\n";
			$msg .= "Content-Type: application/octetstream;\n\tname=\"".$fileName."\"\n";
			$msg .= "Content-Transfer-Encoding: base64\n";
			$msg .= "Content-Disposition: attachment;\n\tfilename=\"".$fileName."\"\n\n";

			 //file goes here
			$fd = fopen ($attmFile, "r");
			$fileContent = fread($fd,filesize($attmFile));
			fclose ($fd);
			$fileContent = chunk_split(base64_encode($fileContent));
			$msg .= $fileContent;
			$msg .= "\n\n";
//		}
	}
	//message ends
	$msg .="\n--".$OB."--\n";
	mail($to,$subject,$msg,$headers); 
}
*/

function sendmail_attach_new($to, $subject, $text, $html, $attmFile, $headers, $from) {
	global $priority, $importance, $mailing_list, $precedence, $unsubscribe, $organization,  $reply_to;
	
	$mail = new htmlMimeMail();
	if ($priority) {
		$mail->headers['Priority'] = $priority;
	}
	if ($importance) {
		$mail->headers['Importance'] = $importance;
	}
	if ($mailing_list) {
		$mail->headers['Mailing-List'] = $mailing_list;
	}
	if ($precedence) {
		$mail->headers['Precedence'] = $precedence;
	}
	if ($unsubscribe) {
		$mail->headers['List-Unsubscribe'] = $unsubscribe;
	}				
	if ($organization) {
		$mail->headers['Organization'] = $organization;
	}
	if ($reply_to) {
		$mail->headers['Reply-to'] = $reply_to;
	}	
	$attachment = $mail->getFile($attmFile);
	if ($text && !$html) {
		$mail->setText($text);
	} 
	if ($html) {
		$mail->setHtml($html, $text);
	}
	$patharray = explode ("/", $attmFile); 
	$fileName = $patharray[count($patharray)-1];
	$mail->addAttachment($attachment, $fileName, 'application/octetstream');
	$mail->setFrom($from);
	$mail->setSubject($subject);
    $mail->send(array($to));
}

$Submit = $_REQUEST[Submit];
$total = $_REQUEST[total];

if ($Submit=="Send") {

	$subjectid = $_REQUEST[subjectid]/1;
	if ($subjectid==0) {
		print "<font face=arial size=2>Newsletter not selected. You need to select a newsletter to send :-) <a href=mail.php>Click here to try again</a>.</font>";
	exit;
	}
	$groupid = $_REQUEST[groupid]/1;
	if ($groupid==0) {
		print "<font face=arial size=2>Group not selected. <a href=mail.php>Click here to try again</a>.</font>";
	exit;
	}
	$batch = $_REQUEST[batch]/1;
	if ($batch==0) {
		print "<font face=arial size=2>Batch size not defined. <a href=mail.php>Click here to try again</a>.</font>";
	exit;
	}

	if ($batch>1000) {
		print "<font face=arial size=2>Batch size too big. <a href=mail.php>Click here to try again</a>.</font>";
	exit;
	}
	$mid = trim($_REQUEST[mid]);
	if ($mid=="") {
		print "<font face=arial size=2>Please enter a unique id for this newsletter mail out. <a href=mail.php>Click here to try again</a>.</font>";
	exit;
	}

	// getting content
	$DATA1 = sql_data("select * from $tableNewsletter where id = '$subjectid'");
	$subject_or = stripslashes($DATA1[subject]);
	$body = $DATA1[body];
	$body_text = $DATA1[body_text];
	$attachment = $DATA1[attachment];
	$from_name = $DATA1[from_name];
	$from_email = $DATA1[from_email];
	$priority = $DATA1[priority];
	$importance = $DATA1[importance];
	$mailing_list = $DATA1[mailing_list];
	$precedence = $DATA1[precedence];
	$unsubscribe = $DATA1[unsubscribe];
	$organization = $DATA1[organization];
	$reply_to = $DATA1[reply_to];
	
	$GROUP = sql_data("select name from $tableGroup where id='$groupid'");


	$SQL = "select * from $tableMail where mid!='$mid' and groupid='$groupid' and subscription='1' order by id limit $batch";

	$Result = sql_query($SQL);
	$num_rows = mysql_num_rows($Result);
	if ($num_rows=="0" && $total==0) { 
	?> 
	<center><font face="Arial, Helvetica, sans-serif" size="2"><b>No Record Present<br> Press back your browser button and try again </b></font></center>
	<?php
		include "footer.php";
		exit;
	}
	if ($num_rows=="0" && $total>0) { 
	
	// add log
	?> 
	<center><font face="Arial, Helvetica, sans-serif" size="2"><b>All mailed out!<br>Total: <?php print $total;?><p>
	 </b></font></center>
	<?php
		$data = sql_data("select log from $tableLog where comment='$mid'");
		$logbuffer = $data[log]."Total Mailed: $total";
		sql_query("update $tableLog set log='$logbuffer' where comment='$mid'");
		include "footer.php";
		exit;
	}
	$row=0;
	$logbuffer = "";
	$mailheaders  = "From: $from_name <$from_email>\n";
	$mailheaders .= "MIME-Version: 1.0\n";
	if (strlen($body)>10 || $attachment)	{
		$mailheaders .= "Content-Type: text/html;\n";
	}
	else
	{
		$mailheaders .= "Content-Type: text/plain;\n";
	}
	$mailheaders .= "X-Mailer: BMX\n";

	if (isset($priority) && $priority!="") { $mailheaders .= "Priority: $priority\n"; }
	if (isset($importance) && $importance!="") { $mailheaders .= "Importance: $importance\n"; }
	if (isset($precedence) && $precedence!="") { $mailheaders .= "Precedence: $precedence\n"; }
	if (isset($reply_to) && $reply_to!="") { $mailheaders .= "Reply-to: $reply_to\n"; }
	if (isset($mailing_list) && $mailing_list!="") { $mailheaders .= "Mailing-List: $mailing_list\n"; }
	if (isset($unsubscribe) && $unsubscribe!="") { $mailheaders .= "List-Unsubscribe: $unsubscribe\n"; }
	if (isset($organization) && $organization!="") { $mailheaders .= "Organization: $organization\n"; }


	while($row < $num_rows)
    {
		$DATA = mysql_fetch_array($Result);
		$email= $DATA[email];
		$msg = stripslashes(str_replace("{EMAIL}",$email,$body)); // replace email variable
		$msg_text = stripslashes(str_replace("{EMAIL}",$email,$body_text)); // replace email variable in text body
		$subject = stripslashes(str_replace("{EMAIL}",$email,$subject_or));
		$msg = str_replace("{GROUPNAME}",$GROUP[name],$msg);
		$msg_text = str_replace("{GROUPNAME}",$GROUP[name],$msg_text);
		$subject = str_replace("{GROUPNAME}",$GROUP[name],$subject);
		################ REPLACEMENT #######################
		$msg = str_replace("{TITLE}",$DATA[title],$msg);
		$msg_text = str_replace("{TITLE}",$DATA[title],$msg_text);
		$subject = str_replace("{TITLE}",$DATA[title],$subject);

		$msg = str_replace("{NAME}",$DATA[name],$msg);
		$msg_text = str_replace("{NAME}",$DATA[name],$msg_text);
		$subject = str_replace("{NAME}",$DATA[name],$subject);

		$msg = str_replace("{ADDRESS1}",$DATA[address1],$msg);
		$msg_text = str_replace("{ADDRESS1}",$DATA[address1],$msg_text);
		$subject = str_replace("{ADDRESS1}",$DATA[address1],$subject);

		$msg = str_replace("{ADDRESS2}",$DATA[address2],$msg);
		$msg_text = str_replace("{ADDRESS2}",$DATA[address2],$msg_text);
		$subject = str_replace("{ADDRESS2}",$DATA[address2],$subject);

		$msg = str_replace("{CITY}",$DATA[city],$msg);
		$msg_text = str_replace("{CITY}",$DATA[city],$msg_text);
		$subject = str_replace("{CITY}",$DATA[city],$subject);

		$msg = str_replace("{STATE}",$DATA[state],$msg);
		$msg_text = str_replace("{STATE}",$DATA[state],$msg_text);
		$subject = str_replace("{STATE}",$DATA[state],$subject);

		$msg = str_replace("{PROVINCE}",$DATA[state],$msg);
		$msg_text = str_replace("{PROVINCE}",$DATA[state],$msg_text);
		$subject = str_replace("{PROVINCE}",$DATA[state],$subject);

		$msg = str_replace("{ZIP}",$DATA[zip],$msg);
		$msg_text = str_replace("{ZIP}",$DATA[zip],$msg_text);
		$subject = str_replace("{ZIP}",$DATA[zip],$subject);

		$msg = str_replace("{POSTALCODE}",$DATA[zip],$msg);
		$msg_text = str_replace("{POSTALCODE}",$DATA[zip],$msg_text);
		$subject = str_replace("{POSTALCODE}",$DATA[zip],$subject);

		$msg = str_replace("{COUNTRY}",$DATA[country],$msg);
		$msg_text = str_replace("{COUNTRY}",$DATA[country],$msg_text);
		$subject = str_replace("{COUNTRY}",$DATA[country],$subject);

		$msg = str_replace("{COMPANY}",$DATA[company],$msg);
		$msg_text = str_replace("{COMPANY}",$DATA[company],$msg_text);
		$subject = str_replace("{COMPANY}",$DATA[company],$subject);

		$msg = str_replace("{JOBTITLE}",$DATA[job],$msg);
		$msg_text = str_replace("{JOBTITLE}",$DATA[job],$msg_text);
		$subject = str_replace("{JOBTITLE}",$DATA[job],$subject);

		$msg = str_replace("{WEBURL}",$DATA[url],$msg);
		$msg_text = str_replace("{WEBURL}",$DATA[url],$msg_text);
		$subject = str_replace("{WEBURL}",$DATA[url],$subject);

		$msg = str_replace("{TELEPHONE}",$DATA[phone],$msg);
		$msg_text = str_replace("{TELEPHONE}",$DATA[phone],$msg_text);
		$subject = str_replace("{TELEPHONE}",$DATA[phone],$subject);

		$msg = str_replace("{FAX}",$DATA[fax],$msg);
		$msg_text = str_replace("{FAX}",$DATA[fax],$msg_text);
		$subject = str_replace("{FAX}",$DATA[fax],$subject);

		$msg = str_replace("{FIELD1}",$DATA[field1],$msg);
		$msg_text = str_replace("{FIELD1}",$DATA[field1],$msg_text);
		$subject = str_replace("{FIELD1}",$DATA[field1],$subject);

		$msg = str_replace("{FIELD2}",$DATA[field2],$msg);
		$msg_text = str_replace("{FIELD2}",$DATA[field2],$msg_text);
		$subject = str_replace("{FIELD2}",$DATA[field2],$subject);

		$msg = str_replace("{FIELD3}",$DATA[field3],$msg);
		$msg_text = str_replace("{FIELD3}",$DATA[field3],$msg_text);
		$subject = str_replace("{FIELD3}",$DATA[field3],$subject);

		$msg = str_replace("{FIELD4}",$DATA[field4],$msg);
		$msg_text = str_replace("{FIELD4}",$DATA[field4],$msg_text);
		$subject = str_replace("{FIELD4}",$DATA[field4],$subject);

		$msg = str_replace("{FIELD5}",$DATA[field5],$msg);
		$msg_text = str_replace("{FIELD5}",$DATA[field5],$msg_text);
		$subject = str_replace("{FIELD5}",$DATA[field5],$subject);

		####################################################
		
		$msg = ereg_replace("(\r\n|\n|\r)", "\n",$msg);
		$msg_text = ereg_replace("(\r\n|\n|\r)", "\n",$msg_text);

		if (file_exists($attachment)) {
			sendmail_attach_new($email, $subject, $msg_text, $msg, $attachment, $mailheaders, "$from_name <$from_email>");
		} elseif ($msg) {
			if( ini_get('safe_mode') ){
				mail($email, $subject, $msg, $mailheaders);
			}else{
				mail($email, $subject, $msg, $mailheaders, "-f$from_email");
			}
		} elseif ($msg_text) {
			if( ini_get('safe_mode') ){
				mail($email, $subject, $msg_text, $mailheaders);
			}else{
				mail($email, $subject, $msg_text, $mailheaders, "-f$from_email");
			}
		}
		//update the mailing list for confirmation of sent mail
		$sql = "update $tableMail set mid ='$mid' where id='$DATA[id]'";
		sql_query($sql);
		$logbuffer .= "Mailed: $email<br>";
		$row++;
	}
	if (sql_exist("select id from $tableLog where comment='$mid'")) {
		$data = sql_data("select log from $tableLog where comment='$mid'");
		$logbuffer = $data[log].$logbuffer;
		sql_query("update $tableLog set log='$logbuffer' where comment='$mid'");
	} else {
		sql_query("insert into $tableLog (log, date, comment) values ('$logbuffer',sysdate(), '$mid')");
	}
	
	//$sql = "insert into";
	
	$total = $total+$row;
	$auto = $_REQUEST[auto];
	$delay = $_REQUEST[delay];
	if ($auto==1)	{

		header("Location: automail.php?total=$total&mid=$mid&batch=$batch&groupid=$groupid&subjectid=$subjectid&auto=1&delay=$delay");
		exit;
	}
	else
	{ 
	$subjectid = $_REQUEST[subjectid];
	$groupid = $_REQUEST[groupid];
	$mid = $_REQUEST[mid];
	$batch = $_REQUEST[batch];

	?>
<form method="post" action="mail.php">
<input type=hidden name=subjectid value="<?php print $subjectid;?>">
<input type=hidden name=groupid value="<?php print $groupid;?>">
<input type=hidden name=mid value="<?php print $mid;?>">
<input type=hidden name=batch value="<?php print $batch;?>">
<input type=hidden name=total value="<?php print $total;?>">

  <table width="60%" border="0" cellspacing="1" cellpadding="1" align="center" bgcolor=#DCDCDC>
<tr><td colspan=2><font face=arial size=2><strong>Bulk Mailer</strong></font></td></tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Subject:</font></td>
      <td> 
	  <font face=arial size=2>
				<em><?php 
				$SQL = "select subject from $tableNewsletter where id=$subjectid";
                $data = sql_data($SQL);
				print $data[subject];
                ?></em>
				</font>
      </td>
    </tr>
<tr>
<td align=right><font face=arial size=2>Group:</font></td>
<td>
<font face=arial size=2><em>
<?php
				$SQL = "select name from $tableGroup where id=$groupid";
                $data = sql_data($SQL);
				print $data[name];
?></em>
</td>
</tr>
<tr>
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Total Mailed:</font></td>
      <td> 
	  <font face=arial size=2>
	<em><?php print $total; ?></em>
	</font>
      </td>
    </tr>

    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Mailer ID:</font></td>
      <td> 
        <font face=arial size=2><em><?php print $mid; ?></em></font>
      </td>
    </tr>
    <tr>
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Auto 
        Mail:</font></td>
      <td>
        <input type="radio" name="auto" value="1">
        <font size="2" face="Arial, Helvetica, sans-serif">Yes</font> 
        <input type="radio" name="auto" value="0" checked>
        <font size="2" face="Arial, Helvetica, sans-serif"> No </font></td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Next Batch Size:</font></td>
      <td> 
        <input type="text" name="batch" value="<?php print $batch; ?>" size=5>
		<font face=arial size=2>(emails per batch: in one go)</font>
      </td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Delay b/w Batch:</font></td>
      <td> 
        <font face=arial size=2><input type=text name=delay value="10"> <em>(seconds delay between batch sends, if set to auto)</em></font>
      </td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">&nbsp;</font></td>
      <td> 
        <input type="submit" name="Submit" value="Send">
      </td>
    </tr>
  </table>
</form>
<div align=center>
<font face=arial size=2 color=red>Do not close or leave this window. Mailing is in progress with unique id as above.</font>
</div>
	<?php

		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Bulk Mailer</title>
</head>

<body>

<form name="mail" method="post" action="mail.php">
<?php $mid = uniqid("");?>

  <table width="60%" border="0" cellspacing="1" cellpadding="1" align="center" bgcolor=#DCDCDC>
<tr><td colspan=2><font face=arial size=2><strong>Bulk Mailer</strong></font></td></tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Subject:</font></td>
      <td> 
        		<select size="1" name="subjectid" style="width=250">
				<option value="">-- Select -- 
				<?php 
				$SQL = "select id, subject from $tableNewsletter order by id";
                DropDownOptionsSelect($SQL, $subjectid);
                ?>
				</select>
      </td>
    </tr>
<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptionsSelect("select id,name from $tableGroup order by name",$groupid);
?>
</select>
</td>
</tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Mailer ID: <a href="javascript:openWindow('help.php?id=10')">?</a></font></td>
      <td> 
        <font face=arial size=2><input type=text name=mid value="<?php print $mid; ?>"> <em>(unique id for this newsletter mail out)</em></font>
      </td>
    </tr>
    <tr>
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Auto 
        Mail: <a href="javascript:openWindow('help.php?id=9')">?</a></font></td>
      <td>
        <input type="radio" name="auto" value="1">
        <font size="2" face="Arial, Helvetica, sans-serif">Yes</font> 
        <input type="radio" name="auto" value="0" checked>
        <font size="2" face="Arial, Helvetica, sans-serif"> No </font></td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Batch Size: <a href="javascript:openWindow('help.php?id=8')">?</a></font></td>
      <td> 
        <input type="text" name="batch" value="5" size=5> <font face=arial size=2>(emails per batch: in one go per delay seconds)</font>
      </td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Delay b/w Batch:</font></td>
      <td> 
        <font face=arial size=2><input type=text name=delay value="10"> <em>(seconds delay between batch sends, if set to auto)</em></font>
      </td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">&nbsp;</font></td>
      <td> 
        <input type="submit" name="Submit" value="Send">
      </td>
    </tr>
  </table>
</form>
<p>

<?php include "footer.php";?>
</font>
</body>
</html>
