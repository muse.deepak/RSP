<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

$search = $_POST[search];
$groupid1 = $_POST[groupid1];
$groupid2 = $_POST[groupid2];

if ($search=="Search") {
	if (($groupid1==0) || ($groupid2==0) || (($groupid1==$groupid2))) {
		print "<font face=arial size=2>Two separate groups not selected. Try again by clicking <a href=duplicates.php>here</a>.";
		include "footer.php";
		exit;
	}
	
	$sql = " select distinct t1.email from $tableMail t2, $tableMail t1 where (t1.email=t2.email and t1.groupid='$groupid1' and t2.groupid='$groupid2' );";
	$Result = sql_query($sql);
	$num_rows = mysql_num_rows($Result);
	$row=0;
	if ($num_rows>1) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Search Result</title>
</head>

<body>
<font face=arial size=2>

<?php
		while($row < $num_rows)
	    {
			$DATA = mysql_fetch_array($Result);
			print $DATA[0]."<br>";
			$row++;
		}
		include "footer.php"; exit; 
	} else {
	?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Search Result</title>
</head>

<body>
<font face=arial size=2>
No email address found. <a href="search.php">Click here</a> to try again.
<?php include "footer.php"; exit; }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Search Duplicates</title>
</head>

<body>

<form action=duplicates.php method=post>
<input type=hidden name=search value="Search">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Search Duplicate Email Addresses in Two Lists</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Group 1:</font></td>
<td>
<select name="groupid1">
<option value=0>-- Select a Group --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptions($sql);
?>
</select>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Group 2:</font></td>
<td>
<select name="groupid2">
<option value=0>-- Select a Group --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptions($sql);
?>
</select>
</td>
</tr>
<tr><td colspan=2 align=right><input type="submit" name="search" value="Search"></td></tr>
</table>
</form>

<?php include "footer.php"; ?>
</body>
</html>
