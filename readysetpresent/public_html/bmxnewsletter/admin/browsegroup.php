<?php
// Bulk Mailer X (BMX)
// October 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
$configList = "per_page";
include "mysql.php";

$id = $_GET[id]/1;

if ($id==0) {
	print "<font face=arial size=2>No group selected. You need to select a group to browse. <a href=groups.php>Click here to try again</a>.</font>";
	exit;
}

	$grouptotal = sql_data("select count(id) from $tableMail where groupid=$id");
	$grouptotal = $grouptotal[0];
	$groupname = sql_data("select name from $tableGroup where id=$id");
	$groupname = $groupname[0];

	$page = $_GET[page]/1;
	if ($page==0) $page = 1;
	$start = ($page-1)*$per_page;
	$result = sql_query("select id,email,subscription from $tableMail where groupid=$id order by id limit $start,$per_page");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Browse Group : <?php print $groupname; ?></title>
</head>

<body>
<font face=arial size=2>

<div align=center><h2><em><?php print $groupname;?></em></h2></div>
<table border=0 width=75% align=center>
<tr bgcolor=#DCDCDC>
<td align=center><font face=arial size=2><strong>Subscription</strong> <a href="javascript:openWindow('help.php?id=6')">?</a></td><td align=center><strong><font face=arial size=2>Email Addresses</font></strong></td><td></td>
</tr>
<?php 
$counter = (($page-1)*$per_page)+1;
while ($row = mysql_fetch_array($result)) { 
 if ($counter%2==0) { $bgcolor="ffffcc"; } else { $bgcolor="ecfafb"; }
?>
<tr bgcolor="#<?php print $bgcolor?>">
<td><font face=arial size=2><?php print $counter;?>.</font>&nbsp;&nbsp;
<?php if ($row[subscription]==0) print "X"; ?>
</td><td><font face=arial size=2><a href=editemail.php?id=<?php print $row[id]?>><?php print $row[email];?></a></font></td><td><font face=arial size=2>[<a href=deleteemail.php?id=<?php print $row[id];?>&page=<?php print $page;?> onclick="javascript:return confirm('Are you sure you want to delete it permanently?\n\n<?php print $row[email];?>');">delete</a>]</td>
</tr>
<?php 
$counter++;
} ?>
<tr bgcolor=#DCDCDC>
<td><font face=arial size=2>
<?php if ($page>1) { $newpage = $page-1; print "<a href=browsegroup.php?id=$id&page=$newpage>"; }?>
&lt; Previous
<?php if ($page>1) { print "</a>"; }?>
</font></td><td align=center><font face=arial size=2><a href=browsegroup.php?id=<?php print $id; ?>>Home</a></font></td><td align=right><font face=arial size=2>
<?php
$totalpages = $grouptotal/$per_page;
if ($totalpages>$page) { $newpage = $page+1; print "<a href=browsegroup.php?id=$id&page=$newpage>"; }
?>
Next &gt;
<?php if ($totalpages>$page) { print "</a>"; }?>
</font></td>
</tr>
</table>

<p>
<?php include "footer.php";?>
</font>
</body>
</html>
