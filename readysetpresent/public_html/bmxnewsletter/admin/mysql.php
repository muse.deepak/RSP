<?php
// General purpose MySQL functions
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include_once "config.php";

	$showerror = 1; // set to 0 if you do not want to print sql queries on web in case of error
	error_reporting(7); // good level
	global $SQLID;
	$SQLID = sql_connect(); // auto-connection whenver this file is called

function sql_connect() {
	global $db_host, $db_user, $db_pass, $db;
    $SQLID = @mysql_connect($db_host, $db_user, $db_pass) or die ("Unable to connect to database server! Check your configuration.");
    @mysql_select_db($db, $SQLID) or die ("Unable to get database. Please check your configuration!");
    return $SQLID;
}

function sql_query($sql) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	return $result;
}

function sql_error_check($result, $sql) {
	global $showerror;
    if(!$result) { 
		print "DB Error: ".mysql_errno().": ".mysql_error()."<BR>"; 
			if ($showerror) print $sql; 
	}
}

function sql_exist($sql) {
        $result=mysql_query($sql);
		sql_error_check($result, $sql);
        $numrows=mysql_num_rows($result);
        if($numrows>0) { return true; }
		else { return false; }
}

function sql_insert_id($sql) {
	global $SQLID;
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	return mysql_insert_id($SQLID);
}

function sql_data($sql) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	$DATA = mysql_fetch_array($result);
	return $DATA;
}

function DropDownOptions($sql) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	while ($row = mysql_fetch_array($result)) {
	    print "<option value=$row[0]>$row[1]";
   }
}

function DropDownOptionsTwo($sql) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	while ($row = mysql_fetch_array($result)) {
		print "<option value=$row[0]>$row[1] $row[2]";
	}
}

function DropDownOptionsThree($sql) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	while ($row = mysql_fetch_array($result)) {
    	print "<option value=$row[0]>$row[1] $row[2] $row[3]";
	}
}

function DropDownOptionsSelect($sql, $itemID) {
	$result = mysql_query($sql);
	while ($row = mysql_fetch_array($result)) {
		print "<option value=$row[0]";
	    if ($itemID==$row[0]) { print " SELECTED";}
	    print ">$row[1]";
	}
}

function DropDownOptionsTwoSelect($sql, $itemID) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	while ($row = mysql_fetch_array($result)) {
		print "<option value=$row[0]";
    	if ($itemID==$row[0]) { print " SELECTED";}
    	print ">$row[1] $row[2]";
   }
}

function DropDownOptionsOne($sql) {
	$result = mysql_query($sql);
	sql_error_check($result, $sql);
	while ($row = mysql_fetch_array($result)) {
		print "<option>$row[0]";
	}
}


// get variables needed in context

if ($configList!="") {
	$configList=str_replace(',', "','", addslashes($configList));
	$result = sql_query("select keyname,value from $tableConfig where keyname in ('$configList')");
	$buffer = ""; $buffer2 = "";
	while ($row = mysql_fetch_array($result)) {
			$key = addslashes($row['keyname']);
			$value= addslashes($row['value']);
			$buffer .= "\$$key = \"$value\";";
			$buffer2 .= "\$$key = stripslashes(\$$key);\n";
			//print $buffer."<br>";
	}
	eval($buffer);
	eval($buffer2);

}

?>