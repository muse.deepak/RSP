<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

	include "checksession.php";
	
	$id = $_REQUEST[id]/1;
	if ($id==0) { Header("Location: newsletters.php"); exit; }
	include "mysql.php";
	
	$update = $_POST[update];
	
if ($update=="Update") {
	$from_name = addslashes($_POST[from_name]);
	$from_email = addslashes($_POST[from_email]);
	$subject = addslashes($_POST[subject]);
	$body = addslashes($_POST[body]);
	$body_text = addslashes($_POST[body_text]);
	$attachment = addslashes($_POST[attachment]);
	$priority = addslashes($_POST[priority]);
	$importance = addslashes($_POST[importance]);
	$mailing_list = addslashes($_POST[mailing_list]);
	$precedence = addslashes($_POST[precedence]);
	$unsubscribe = addslashes($_POST[unsubscribe]);
	$organization = addslashes($_POST[organization]);
	$reply_to = addslashes($_POST[reply_to]);

	sql_query("update $tableNewsletter set from_name='$from_name', from_email='$from_email', subject='$subject', body='$body', body_text='$body_text', attachment='$attachment', priority='$priority', importance='$importance', mailing_list='$mailing_list', precedence='$precedence', unsubscribe='$unsubscribe', organization='$organization', reply_to='$reply_to' where id=$id");
	$microtime = time();
	Header("Location: newsletters.php?refresh=$microtime");
	exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX : Newsletters</title>
</head>

<body>
<font face=arial size=2>
<?php
$data = sql_data("select * from $tableNewsletter where id=$id");
?>
<p>
<form action=editnewsletter.php method=post>
<input type=hidden name=update value="Update">
<input type=hidden name=id value="<?php print $id;?>">
<table border=0 bgcolor=#DCDCDC>
<tr><td colspan=2><font face=arial size=2><strong>Edit Newsletter</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>From Name:</font></td>
<td><input name=from_name value="<?php print stripslashes($data[from_name]); ?>" size=40 maxlength=50></td>
</tr>
<tr>
<td align=right><font face=arial size=2>From Email Address:</font></td>
<td><input name=from_email value="<?php print stripslashes($data[from_email]); ?>" size=40 maxlength=60></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Subject:</font></td>
<td><input name=subject value="<?php print stripslashes($data[subject]); ?>" size=40 maxlength=100></td>
</tr>
<tr><td align=right><font face=arial size=2>Message Body (text):</font></td>
<td>
<textarea cols="70" rows="8" name="body_text"><?php print stripslashes($data[body_text]); ?></textarea>
</td>
</tr>
<tr><td align=right><font face=arial size=2>Message Body (HTML):</font></td>
<td>
<textarea cols="70" rows="8" name="body"><?php print stripslashes($data[body]); ?></textarea>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Attachment:</font></td>
<td><input name=attachment value="<?php print $data[attachment]; ?>" size=50 maxlength=90><br><font face=arial size=2>(file name on the server including full path, leave empty if not sending any attachment)</td> 
</tr>
<tr><td colspan=2><hr size=1><font face=arial size=2><strong>Optional Email Headers ?</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Reply-to:</font></td>
<td><input name=reply_to value="<?php print $data[reply_to]?>" size=50 maxlength=100><font face=arial size=2>('Reply-to' takes precedence over 'From')</font></td>
</tr>

<tr>
<td align=right><font face=arial size=2>Organization:</font></td>
<td><input name=organization value="<?php print $data[organization]?>" size=50 maxlength=100></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Mailing-List:</font></td>
<td><input name=mailing_list value="<?php print $data[mailing_list]?>" size=50 maxlength=100></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Priority:</font></td>
<td><select name="priority">
<option value=""> -- Select --
<option <?php if ($data[priority]=="Low") print "SELECTED";?>>Low
<option <?php if ($data[priority]=="Normal") print "SELECTED";?>>Normal
<option <?php if ($data[priority]=="High") print "SELECTED";?>>High
</select></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Importance:</font></td>
<td><select name="importance">
<option value=""> -- Select --
<option <?php if ($data[importance]=="Low") print "SELECTED";?>>Low
<option <?php if ($data[importance]=="Normal") print "SELECTED";?>>Normal
<option <?php if ($data[importance]=="High") print "SELECTED";?>>High
</select></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Precedence:</font></td>
<td><input name=precedence value="<?php print $data[precedence]?>" size=10 maxlength=20><font face=arial size=2>&nbsp;</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>List-Unsubscribe:</font></td>
<td><input name=unsubscribe value="<?php print $data[unsubscribe]?>" size=50 maxlength=100><font face=arial size=2>(e.g. URL or email address)</font></td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="update" value="Update"></td></tr>
</table>
</form>
<p>

<?php include "footer.php";?>
</font>
</body>
</html>
