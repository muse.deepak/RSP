<?php
// Bulk Mailer X (BMX)
// January, 2005. Release 3.1.
// Copyright (c) 1997-2005 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

// if process
$submit = $_REQUEST[submit];
$table = $_REQUEST[table];
$email_field = $_REQUEST[email_field];

if ($submit=="Process" && $table!="" && $email_field!="") {

$groupid = $_REQUEST[groupid]/1;
if ($groupid==0) {
	print "<font face=arial size=2>No group selected. You need to select a group to add email addresses. <a href=insert.php>Click here to try again</a>.</font>";
	exit;
}

	mysql_query("select $email_field from $table limit 1") or die("<font face=arial size=2>Invalid table or email field. <a href=importfromdb.php>Click here</a> to try again.</font>");

	$SQL = "select $email_field from $table";
	$Result = sql_query($SQL);
	$num_rows = mysql_num_rows($Result);
	if ($num_rows=="0") { 
	?>
	<center><font face="Arial, Helvetica, sans-serif" size="2"><b>No Record Present <br> Press back your browser button and try again </b></font></center>
	<?php
		exit;
	}

$duplicates = 0;
$added = 0;
$invalid = 0;
$log = "";
$c=1;
include "checkemail.php";
ob_end_flush();
ob_clean();
flush();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Insert Emails</title>
</head>

<body>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
<?php
print "......................................... \n<br>";
print "We are now processing the database table.... \n<br>";
print "......................................... \n<br>";
flush();
	while($row = mysql_fetch_array($Result))
    {
		$email= $row[0];
		if (!check_email($email)) {  $log .="INVALID: $email<br>";
$invalid++; }
		else if (sql_exist("select email from $tableMail where email='$email' and groupid='$groupid'")) { $log .= "DUPLICATE: $email<br>"; $duplicates++; }
		else { sql_query("insert into $tableMail (email, groupid) values ('$email', $groupid)"); $added++; }
		
		if ($c%10==0) {
			print ".";
		}
		if ($c%100==0) {
			print " $c processed | new: $added | duplicate: $duplicates | invalid: $invalid | <br>\n";
		}
		$c++;
		flush();
		ob_flush(); 
	}
	include "processcomplete.php";
	process_complete($invalid, $duplicates, $added, $groupid, $log);
	include "footer.php";
	ob_end_flush();
	exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Import Emails from DB</title>
</head>

<body>


<form method="post" action="importfromdb.php">
  <table width="60%" border="0" cellspacing="1" cellpadding="1" align="center" bgcolor=#DCDCDC>
	<tr><td colspan=2><font face=arial size=2><strong>Import from MySQL Database</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptions("select id,name from $tableGroup order by name");
?>
</select>
</td>
</tr>

    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Table 
        Name: </font></td>
      <td>
        <input type="text" name="table" value="<?php print $table; ?>">
      </td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">Email Field: </font></td>
      <td>
        <input type="text" name="email_field" value="<?php print $email_field; ?>">
      </td>
    </tr>
    <tr> 
      <td align="right"><font face="Arial, Helvetica, sans-serif" size="2">&nbsp;</font></td>
      <td align="right">
        <input type="submit" name="submit" value="Process">
      </td>
    </tr>
  </table>
</form>
<blockquote>
<font face=arial size=2>
<strong>Tips:</strong><br>
Table must be in the same database where Bulk Mail X resides.<br>
New email addresses will be appended to the group you select, and will be subscribed.<br>
<p>
</blockquote>

<?php include "footer.php"; ?>