<?php
// Bulk Mailer X (BMX)
// January, 2005. Release 3.1.
// Copyright (c) 1997-2005 Webx Networks (http://www.webx.net)

	include "checksession.php";
	include "mysql.php";
	$id = $_REQUEST[id]/1;
	if ($id==0) { Header("Location: groups.php"); exit; }
	$data = sql_data("select name from $tableGroup where id=$id");
	$groupname = $data[0];

	$buffer = "";

	$t = $_REQUEST[t];
	if ($t=="") { print "Error 1001"; exit; }
	if ($t=="all") { $SQL = "select email from $tableMail where groupid='$id' order by email"; }
	if ($t=="sub") { $SQL = "select email from $tableMail where groupid='$id' and subscription='1' order by email"; }
	if ($t=="unsub") { $SQL = "select email from $tableMail where groupid='$id' and subscription='0' order by email"; }
	
	$result = sql_query($SQL);
	while ($row = mysql_fetch_array($result)) {
		$buffer .= "$row[email]\r\n";
	}

header("Cache-control: max-age=31536000");
header("Expires: " . gmdate("D, d M Y H:i:s",time()) . "GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s",time()) . "GMT");
header("Content-disposition: attachment; filename=$groupname.txt");
header("Content-Length: ".strlen($buffer));
header("Content-type: text");
print $buffer;
?>