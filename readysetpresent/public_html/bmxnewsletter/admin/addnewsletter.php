<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

$subject = $_POST[subject];
$create = $_POST[create];

if (trim($subject)!="" and $create=="Create") {
	$from_name = addslashes($_POST[from_name]);
	$from_email = addslashes($_POST[from_email]);
	$subject = addslashes($_POST[subject]);
	$body = addslashes($_POST[body]);
	$body_text = addslashes($_POST[body_text]);
	$attachment = addslashes($_POST[attachment]);
	$priority = addslashes($_POST[priority]);
	$importance = addslashes($_POST[importance]);
	$mailing_list = addslashes($_POST[mailing_list]);
	$precedence = addslashes($_POST[precedence]);
	$unsubscribe = addslashes($_POST[unsubscribe]);
	$organization = addslashes($_POST[organization]);
	$reply_to = addslashes($_POST[reply_to]);

	$sql = "insert into $tableNewsletter (from_name, from_email, subject, body, body_text, attachment, priority, importance, mailing_list, precedence, unsubscribe, organization, reply_to) values ('$from_name', '$from_email', '$subject', '$body', '$body_text', '$attachment', '$priority', '$importance', '$mailing_list', '$precedence', '$unsubscribe', '$organization', '$reply_to')";
	sql_query($sql);
	$microtime = time();
	Header("Location: newsletters.php?refresh=$microtime");
	exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Newsletters: Add</title>
</head>

<body>

<p>
<form action=addnewsletter.php method=post>
<input type=hidden name=create value="Create">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Create Newsletter</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>From Name:</font></td>
<td><input name=from_name value="" size=30 maxlength=50> <font face=arial size=2>(only name or company name)</td> 
</tr>
<tr>
<td align=right><font face=arial size=2>From Email Address:</font></td>
<td><input name=from_email value="" size=30 maxlength=60> <font face=arial size=2>(only single return email address)</td> 
</tr>
<tr>
<td align=right><font face=arial size=2>Subject:</font></td>
<td><input name=subject value="" size=30 maxlength=100> <font face=arial size=2>(that appears on emails)</td>
</tr>
<tr><td align=right><font face=arial size=2>Message Body (text): <a href="javascript:openWindow('help.php?id=7')">?</a></font></td>
<td>
<textarea cols="70" rows="8" name="body_text"></textarea>
</td>
</tr>
<tr><td align=right><font face=arial size=2>Message Body (HTML): <a href="javascript:openWindow('help.php?id=7')">?</a></font></td>
<td>
<textarea cols="70" rows="8" name="body"></textarea>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Attachment: <a href="javascript:openWindow('help.php?id=15')">?</a></font></td>
<td><input name=attachment value="" size=50 maxlength=90><br><font face=arial size=2>(file name on the server including full path, leave empty if not sending any attachment)</td> 
</tr>
<tr><td colspan=2><hr size=1><font face=arial size=2><strong>Optional Email Headers <a href="javascript:openWindow('help.php?id=11')">?</a></strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Reply-to:</font></td>
<td><input name=reply_to value="" size=50 maxlength=100><font face=arial size=2>('Reply-to' takes precedence over 'From')</font></td>
</tr>

<tr>
<td align=right><font face=arial size=2>Organization:</font></td>
<td><input name=organization value="" size=50 maxlength=100></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Mailing-List:</font></td>
<td><input name=mailing_list value="" size=50 maxlength=100></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Priority:</font></td>
<td><select name="priority">
<option value=""> -- Select --
<option>Low
<option>Normal
<option>High
</select></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Importance:</font></td>
<td><select name="importance">
<option value=""> -- Select --
<option>Low
<option>Normal
<option>High
</select></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Precedence:</font></td>
<td><input name=precedence value="bulk" size=10 maxlength=20><font face=arial size=2>&nbsp;</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>List-Unsubscribe:</font></td>
<td><input name=unsubscribe value="" size=50 maxlength=100><font face=arial size=2>(e.g. URL or email address)</font></td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="create" value="Create"></td></tr>
</table>
</form>
<p>
<?php include "footer.php";?>
</font>
</body>
</html>
