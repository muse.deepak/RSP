<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)
	include "checksession.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Newsletters</title>
</head>

<body>
<font face=arial size=2>
<strong>Newsletters:</strong>
<table border=0 width=90% bgcolor=#DCDCDC align=center>

<?php
include "mysql.php";
$result = sql_query("select id,subject from $tableNewsletter order by subject");

while ($row = mysql_fetch_array($result)) {
	print "<tr><td><font face=arial size=2>[<a href=editnewsletter.php?id=$row[id]>edit</a>]</font></td>";
	print "<td><a href=viewnewsletter.php?id=$row[id]>$row[subject]</a></li></td>";
	print "<td><font face=arial size=2>[<a href=deletenewsletter.php?id=$row[id] onclick=\"javascript:return confirm('Are you sure you want to delete it permanently?');\">delete</a>]</font></td></tr>";
}
?>
</table>

<?php include "footer.php";?>
</font>
</body>
</html>
