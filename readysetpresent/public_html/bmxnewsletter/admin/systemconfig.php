<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";

function success() {
?>
<HTML>
<BODY>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript"><!--
  window.alert("System Configuration updated successfully.");
  location.href='index.php';
  //-->
</SCRIPT>
	<DIV align="center"><FONT face="arial" size="2">
System Configuration updated successfully. <A href="index.php">Click here to continue...</A>
</FONT>
	</DIV>
</BODY>
</HTML>
<?php
exit;
}

$update = $_REQUEST[update];

if ($update=="Update1") {

	include "mysql.php";
	sql_query("update $tableConfig set value='".addslashes($_POST[per_page])."' where keyname='per_page'");
	sql_query("update $tableConfig set value='".addslashes($_POST[BMXURL])."' where keyname='BMXURL'");
	sql_query("update $tableConfig set value='".addslashes($_POST[redirectSubscribe])."' where keyname='redirectSubscribe'");
	sql_query("update $tableConfig set value='".addslashes($_POST[redirectUnSubscribe])."' where keyname='redirectUnSubscribe'");
	sql_query("update $tableConfig set value='".addslashes($_POST[redirectSuccess])."' where keyname='redirectSuccess'");
	sql_query("update $tableConfig set value='".addslashes($_POST[verifyRedirect])."' where keyname='verifyRedirect'");
	success();
}

if ($update=="Update2") {

	include "mysql.php";
	sql_query("update $tableConfig set value='".addslashes($_POST[header])."' where keyname='header'");
	sql_query("update $tableConfig set value='".addslashes($_POST[footer])."' where keyname='footer'");
	success();
}

if ($update=="Update3") {
	include "mysql.php";
	sql_query("update $tableConfig set value='".addslashes($_POST[E_VERIFY_SUBS_SUBJECT])."' where keyname='E_VERIFY_SUBS_SUBJECT'");
	sql_query("update $tableConfig set value='".addslashes($_POST[E_VERIFY_SUBS])."' where keyname='E_VERIFY_SUBS'");
	sql_query("update $tableConfig set value='".addslashes($_POST[E_VERIFY_UNSUBS_SUBJECT])."' where keyname='E_VERIFY_UNSUBS_SUBJECT'");
	sql_query("update $tableConfig set value='".addslashes($_POST[E_VERIFY_UNSUBS])."' where keyname='E_VERIFY_UNSUBS'");
	success();
}


if ($update=="Update4") {
	include "mysql.php";
	sql_query("update $tableConfig set value='".addslashes($_POST[M_GROUP_INVALID])."' where keyname='M_GROUP_INVALID'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_EMAIL_INVALID])."' where keyname='M_EMAIL_INVALID'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_ALREADY_SUBSCRIBED])."' where keyname='M_ALREADY_SUBSCRIBED'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_ENTER_VALID_EMAIL])."' where keyname='M_ENTER_VALID_EMAIL'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_REQUEST_RECEIVED])."' where keyname='M_REQUEST_RECEIVED'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_NOT_SUBS])."' where keyname='M_NOT_SUBS'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_REQUEST_UNSUB])."' where keyname='M_REQUEST_UNSUB'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_THANK_YOU_LINK])."' where keyname='M_THANK_YOU_LINK'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_INVALID_HEAD])."' where keyname='M_INVALID_HEAD'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_INVALID_BODY])."' where keyname='M_INVALID_BODY'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_SUBS_HEAD])."' where keyname='M_SUBS_HEAD'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_SUBS_BODY])."' where keyname='M_SUBS_BODY'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_UNSUBS_HEAD])."' where keyname='M_UNSUBS_HEAD'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_UNSUBS_BODY])."' where keyname='M_UNSUBS_BODY'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_SUBS_DUPLICATE])."' where keyname='M_SUBS_DUPLICATE'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_SUBS_DUP_BODY])."' where keyname='M_SUBS_DUP_BODY'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_UNSUBS_NOTFOUND])."' where keyname='M_UNSUBS_NOTFOUND'");
	sql_query("update $tableConfig set value='".addslashes($_POST[M_UNSUBS_NOT_BODY])."' where keyname='M_UNSUBS_NOT_BODY'");

	success();
}

if ($update=="Update5") {
	
	$groupid = $_POST[groupid]/1;
	if ($groupid==0) { 
		print "<font face=arial size=2>Error: Group not selected. Invalid Value. <a href=systemconfig.php>Try again</a>!</font>";
		exit;
	}
	?>
<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Static HTML Code</TITLE>
</HEAD>
<BODY>
<FONT face="arial" size="2">
<h3>Static HTML Code</h3>
These are sample HTML code for this particular group. This will give you the basic idea how you
can put static HTML code in any of your page. You can make it as fancy as you like, however 
following is the basic form: (<font color=red>process.php</font> is part of public module, <font color=red>group</font> defines the Group ID,
<font color=red>action</font> is subscribe or unsubscribe.)
</font>
<hr size="1" width="75%">
<pre>
&lt;form method="post" action="<font color=red>/path/to/process.php</font>"&gt;
&lt;input type="hidden" name="action" value="<font color=red>subscribe</font>"&gt;
&lt;input type="hidden" name="group" value="<font color=red><?php print $groupid;?></font>"&gt;
&lt;INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"&gt;
&lt;/form&gt;
</pre>
<FONT face="arial" size="2">
<hr size="1" width="75%">
<strong><font color=blue>Subscribe + Un-Subscribe</font> HTML Code without submit button:</strong><br>
<textarea cols="70" rows="8">
<form method="post" action="process.php">
<input type="hidden" name="group" value="<?php print $groupid;?>">
<input type="radio" name="action" value="subscribe"> Subscribe
<input type="radio" name="action" value="unsubscribe"> Unsubscribe
<INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"> 
</form>
</textarea>
<p>
<strong>Subscribe + Un-Subscribe HTML Code <font color=blue>with</font> submit button:</strong> (rename the button whatever you like)<br>
<textarea cols="70" rows="8">
<form method="post" action="process.php">
<input type=hidden name=group value="<?php print $groupid;?>">
<input type="radio" name="action" value="subscribe"> Subscribe
<input type="radio" name="action" value="unsubscribe"> Unsubscribe
<INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"> 
<input type="submit" name="newsletter" value="Newsletter">
</form>
</textarea>
<p>
<strong><font color=blue>Subscribe</font> HTML Code without submit button:</strong><br>
<textarea cols="70" rows="8">
<form method="post" action="process.php">
<input type="hidden" name="action" value="subscribe">
<input type="hidden" name="group" value="<?php print $groupid;?>">
<INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"> 
</form>
</textarea>
<p>
<strong>Subscribe HTML Code with submit button:</strong> (rename the button whatever you like)<br>
<textarea cols="70" rows="8">
<form method="post" action="process.php">
<input type=hidden name=action value="subscribe">
<input type=hidden name=group value="<?php print $groupid;?>">
<INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"> 
<input type="submit" name="newsletter" value="Subscribe Newsletter">
</form>
</textarea>
<p>
<strong><font color=blue>Un-Subscribe</font> HTML Code without submit button:</strong><br>
<textarea cols="70" rows="8">
<form method="post" action="process.php">
<input type="hidden" name="action" value="unsubscribe">
<input type="hidden" name="group" value="<?php print $groupid;?>">
<INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"> 
</form>
</textarea>
<p>
<strong>Un-Subscribe HTML Code with submit button:</strong> (rename the button whatever you like)<br>
<textarea cols="70" rows="8">
<form method="post" action="process.php">
<input type=hidden name=action value="unsubscribe">
<input type=hidden name=group value="<?php print $groupid;?>">
<INPUT name="email" size="20" MAXLENGTH="70" type="text" value="Your Email Address"> 
<input type="submit" name="newsletter" value="Unsubscribe Newsletter">
</form>
</textarea>
<HTML><HEAD></font>

<?php
include "footer.php";
exit;
}


?>
<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">


<TITLE>System Configuration</TITLE>
</HEAD>
<BODY>
<FONT face="arial" size="2">
<?php
$con = $_REQUEST[con];
switch($con) {
	case "1":
	$configList = "per_page,BMXURL,redirectSubscribe,redirectUnSubscribe,redirectSuccess,verifyRedirect";
	include "mysql.php";
?>
		<FORM action="systemconfig.php" method="post">			
			<INPUT type="hidden" name="update" value="Update1">
			<TABLE border="0" bgcolor="#DCDCDC" width="90%" align="center">
				<TR>
					<TD colspan="2" align="center"><FONT face="arial" size="2"><STRONG>System Configuration</STRONG></FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Per Page Email Addresses:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="per_page" value="<?php print $per_page;?>" size=10 maxlength=3>
					</TD>
				</TR>
				<TR>
					<TD colspan="2"><FONT face="arial" size="2">While browsing email addresses in groups, how many to show per page?</FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>'newsletter' Installated Location (URL):</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="BMXURL" value="<?php print $BMXURL;?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD colspan="2"><FONT face="arial" size="2">This is the absolute URL (web site address) where you have installed 'newsletter' directory <strong>(without last forward slash)</strong>.</FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Redirect Subscribe Failure:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="redirectSubscribe" value="<?php print $redirectSubscribe;?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD colspan="2"><FONT face="arial" size="2">
Absolute or relative URL to web page, where you want your visitors to be redirected if subscribe request is invalid (may be to your custom subscribe page?).</FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Redirect Un-Subscribe Failure:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="redirectUnSubscribe" value="<?php print $redirectUnSubscribe;?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD colspan="2"><FONT face="arial" size="2">
Absolute or relative URL to web page, where you want your visitors to be redirected if un-subscribe request is invalid (may be to your custom unsubscribe page?).</FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Redirect Success:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="redirectSuccess" value="<?php print $redirectSuccess;?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD colspan="2"><FONT face="arial" size="2">
Absolute or relative URL to web page, where you want your visitors to be redirected once subscribe or unsubscribe request is successful. You may like to redirect to your home page? or some other thank you page?</FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Verify Redirect:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="verifyRedirect" value="<?php print $verifyRedirect;?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD colspan="2"><FONT face="arial" size="2">
Absolute or relative URL to web page, where you want your visitors to be redirected once verification page has processed the request of subscribe or unsubscribe. When a user receives verification email and visits the web page to verify the request, after processing request; will be redirected to this page.</FONT>
					</TD>
				</TR>

					<TR>
						<TD colspan="2" align="right">							
							<INPUT type="submit" name="save" value="Save Changes">
						</TD>
					</TR>
			</TABLE>
		</FORM>

		<blockquote>
		<strong>Tip:</strong>
		Absolute URL should start with http://<br>
		</blockquote>
	<?php
	break;
	case "2": 
	$configList = "header,footer";
	include "mysql.php";

	?>
			<FORM action="systemconfig.php" method="post">			

			<INPUT type="hidden" name="update" value="Update2">
			<TABLE border="0" bgcolor="#DCDCDC" width="90%" align="center">
				<TR>
					<TD colspan="2" align="center"><FONT face="arial" size="2"><STRONG>Header / Footer Template</STRONG></FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Header:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="10" name="header"><?php print $header; ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Footer:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="10" name="footer"><?php print $footer; ?></textarea>
					</TD>
				</TR>
					<TR>
						<TD colspan="2" align="right">							
							<INPUT type="submit" name="save" value="Save Changes">
						</TD>
					</TR>
			</TABLE>
		</FORM>


	<?php
	break;
	case "3": 
	$configList = "E_VERIFY_SUBS_SUBJECT,E_VERIFY_SUBS,E_VERIFY_UNSUBS_SUBJECT,E_VERIFY_UNSUBS";
	include "mysql.php";

	?>
			<FORM action="systemconfig.php" method="post">			

			<INPUT type="hidden" name="update" value="Update3">
			<TABLE border="0" bgcolor="#DCDCDC" width="90%" align="center">
				<TR>
					<TD colspan="2" align="center"><FONT face="arial" size="2"><STRONG>Verification Email Templates for Public Module <a href="javascript:openWindow('help.php?id=14')">?</a></STRONG>
					<br>Text Format Only</FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Subscribe Subject:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="E_VERIFY_SUBS_SUBJECT" value="<?php print stripslashes($E_VERIFY_SUBS_SUBJECT); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Subscribe Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="10" name="E_VERIFY_SUBS"><?php print stripslashes($E_VERIFY_SUBS); ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Un-Subscribe Subject:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="E_VERIFY_UNSUBS_SUBJECT" value="<?php print stripslashes($E_VERIFY_UNSUBS_SUBJECT); ?>" size=40 maxlength=100>
					</TD>
				</TR>

				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Un-Subscribe Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="10" name="E_VERIFY_UNSUBS"><?php print stripslashes($E_VERIFY_UNSUBS); ?></textarea>
					</TD>
				</TR>
					<TR>
						<TD colspan="2" align="right">							
							<INPUT type="submit" name="save" value="Save Changes">
						</TD>
					</TR>
			</TABLE>
		</FORM>


	<?php
	break;
	case "4": 
	$configList  = "M_GROUP_INVALID,M_EMAIL_INVALID,M_ALREADY_SUBSCRIBED,M_ENTER_VALID_EMAIL";
	$configList .= ",M_REQUEST_RECEIVED,M_NOT_SUBS,M_REQUEST_RECEIVED,M_REQUEST_UNSUB,M_THANK_YOU_LINK";
	$configList .= ",M_INVALID_HEAD,M_INVALID_BODY,M_SUBS_HEAD,M_SUBS_BODY,M_UNSUBS_HEAD,M_UNSUBS_BODY";
	$configList .= ",M_SUBS_DUPLICATE,M_SUBS_DUP_BODY,M_UNSUBS_NOTFOUND,M_UNSUBS_NOT_BODY";
	include "mysql.php";

	?>
			<FORM action="systemconfig.php" method="post">			

			<INPUT type="hidden" name="update" value="Update4">
			<TABLE border="0" bgcolor="#DCDCDC" width="90%" align="center">
				<TR>
					<TD colspan="2" align="center"><FONT face="arial" size="2"><STRONG>Misc. Messages used in Public Module</STRONG></FONT>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Invalid Group:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_GROUP_INVALID" value="<?php print stripslashes($M_GROUP_INVALID); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Invalid Email:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_EMAIL_INVALID" value="<?php print stripslashes($M_EMAIL_INVALID); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Already Subscribed:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_ALREADY_SUBSCRIBED" value="<?php print stripslashes($M_ALREADY_SUBSCRIBED); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Enter Valid Email:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_ENTER_VALID_EMAIL" value="<?php print stripslashes($M_ENTER_VALID_EMAIL); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Email Not on List:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_NOT_SUBS" value="<?php print stripslashes($M_NOT_SUBS); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Subscribe Request Success:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_REQUEST_RECEIVED"><?php print stripslashes($M_REQUEST_RECEIVED); ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Un-Subscribe Request Success:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_REQUEST_UNSUB"><?php print stripslashes($M_REQUEST_UNSUB); ?></textarea>
					</TD>
				</TR>

				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Thank You Link:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_THANK_YOU_LINK" value="<?php print stripslashes($M_THANK_YOU_LINK); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD colspan="2" align="center">
<HR size="1" width="75%">
<FONT face="arial" size="2"><STRONG>Verification Process Messages <a href="javascript:openWindow('help.php?id=13')">?</a></STRONG></FONT>
<HR size="1" width="75%">
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Invalid Request Title:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_INVALID_HEAD" value="<?php print stripslashes($M_INVALID_HEAD); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Invalid Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_INVALID_BODY"><?php print stripslashes($M_INVALID_BODY); ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Subscribe Title:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_SUBS_HEAD" value="<?php print stripslashes($M_SUBS_HEAD); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Subscribe Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_SUBS_BODY"><?php print stripslashes($M_SUBS_BODY); ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>UnSubscribe Title:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_UNSUBS_HEAD" value="<?php print stripslashes($M_UNSUBS_HEAD); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>UnSubscribe Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_UNSUBS_BODY"><?php print stripslashes($M_UNSUBS_BODY); ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Duplicate Subscribe Title:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_SUBS_DUPLICATE" value="<?php print stripslashes($M_SUBS_DUPLICATE); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>Duplicate Subscribe Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_SUBS_DUP_BODY"><?php print stripslashes($M_SUBS_DUP_BODY); ?></textarea>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>UnSubscribe N/A Title:</STRONG></FONT>
					</TD>
					<TD>						
						<INPUT name="M_UNSUBS_NOTFOUND" value="<?php print stripslashes($M_UNSUBS_NOTFOUND); ?>" size=40 maxlength=100>
					</TD>
				</TR>
				<TR>
					<TD align="right"><FONT face="arial" size="2"><STRONG>UnSubscribe N/A Body:</STRONG></FONT>
					</TD>
					<TD>						
<textarea cols="60" rows="3" name="M_UNSUBS_NOT_BODY"><?php print stripslashes($M_UNSUBS_NOT_BODY); ?></textarea>
					</TD>
				</TR>

					<TR>
						<TD colspan="2" align="right">							
							<INPUT type="submit" name="save" value="Save Changes">
						</TD>
					</TR>
			</TABLE>
		</FORM>


	<?php
	break;
	case "5": 
	include "mysql.php";
	?>
	<div align=center>
<form action=systemconfig.php method=post>
<input type=hidden name=update value="Update5">
<table border=0 bgcolor=#DCDCDC>
<tr><td colspan=2><font face=arial size=2><strong>Generate Subscribe/Un-subscribe HTML Code</strong></font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptionsSelect($sql, $groupid);
?>
</select>
</td>
</tr>
					<TR>
						<TD colspan="2" align="right">							
							<INPUT type="submit" name="submit" value="Generate Code">
						</TD>
					</TR>
			</TABLE>
		</FORM>

	<?php
	break;
	default:
	?>

	<strong>BMX System Configuration Options:</strong>
	<ul>
	<li><a href=systemconfig.php?con=1>Main Configuration</a></li>
	<li><a href=systemconfig.php?con=2>Header/Footer for Public Module</a></li>
	<li><a href=systemconfig.php?con=3>Email Templates for Verification</a></li>
	<li><a href=systemconfig.php?con=4>Messages in Public Module</a></li>
	<li><a href=systemconfig.php?con=5>Static HTML Code for Subscribe/Unsubscribe</a></li>
	</ul>

	<?php 
	}
	?>
<P>
<div align=center><a href="systemconfig.php">System Configuration Main Page</a></div>
</FONT>
<?php 
include "footer.php";
?>
</BODY>
</HTML>
