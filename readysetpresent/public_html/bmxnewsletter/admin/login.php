<?php 
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)
error_reporting(0);

session_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Login</title>
<style type="text/css">
<!--
.warningsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-color: red;
	font-size: 10pt; 
	font-style: normal; 
	line-height: normal; 
	font-weight: normal; 
	text-align: justify;
	letter-spacing: normal;
	}	
-->
</style>

</head>

<body>
<form name="login" action="login-process.php" method="post">

<table width="50%" border="0">
		<tr>
			<td align="right" valign="top"><font size="2" face="arial"><b>Login:</b></font></td>
			<td valign="top"><input type="text" size="20" maxlength="50" name="admin" value="<?php print $_SESSION[admin]; ?>">
			<?php if ($_SESSION[loginError]) { print "<br><span class='warningsmall'>$_SESSION[loginError]</span>";}?>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top"><font size="2" face="arial"><b>Password:</b></font></td>
			<td valign="top"><input type="password" size="20" maxlength="20" name="password" value="<?php print $_SESSION[password]; ?>">
			<?php if ($_SESSION[passwordError]) { print "<br><span class='warningsmall'>$_SESSION[passwordError]</span>";}?>
			</td>
		</tr>
		<tr>
		<td colspan="2">
		<div align="right"><INPUT type="submit" value="Login" name="Login"></div>
		</td>
		</tr>
	</table>


<?php
session_unregister("loginError");
session_unregister("passwordError");
session_unregister("admin");
session_unregister("password");
?>
</body>
</html>