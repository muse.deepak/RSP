<?php
// Bulk Mailer X (BMX)
// January, 2005. Release 3.1.
// Copyright (c) 1997-2005 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

if (!sql_exist("select id,name from $tableGroup order by name")) {
	print "<font face=arial size=2>No groups defined. You need a group to add email addresses. <a href=groups.php>Click here to add one</a>.</font>";
	exit;
}

$add = $_REQUEST[add];
$overwrite = $_REQUEST[overwrite];
$duplicate = $_REQUEST[duplicate];


if ($add=="Upload") {

$groupid = $_REQUEST[groupid]/1;
if ($groupid==0) {
	print "<font face=arial size=2>No group selected. You need to select a group to add email addresses. <a href=importfromfile.php>Click here to try again</a>.</font>";
	exit;
}
$terminated = $_REQUEST[terminated];
if ($terminated=="") {
	print "<font face=arial size=2>The fields should be terminated by some character, e.g. a semicolon (;) or comma (,). Please input a valid character. <a href=importfromcsv.php>Click here to try again</a>.</font>";
	exit;

}

$i=1;
while ($i<20) {
	if ($_REQUEST[email]==$i) $sqlstr1[] = "email"; 
	if ($_REQUEST[title]==$i) $sqlstr1[] = "title";
	if ($_REQUEST[name]==$i) $sqlstr1[] = "name";
	if ($_REQUEST[address1]==$i) $sqlstr1[] = "address1";
	if ($_REQUEST[address2]==$i) $sqlstr1[] = "address2";
	if ($_REQUEST[city]==$i) $sqlstr1[] = "city";
	if ($_REQUEST[state]==$i) $sqlstr1[] = "state";
	if ($_REQUEST[zip]==$i) $sqlstr1[] = "zip";
	if ($_REQUEST[country]==$i) $sqlstr1[] = "country";
	if ($_REQUEST[company]==$i) $sqlstr1[] = "company";
	if ($_REQUEST[job]==$i) $sqlstr1[] = "job";
	if ($_REQUEST[url]==$i) $sqlstr1[] = "url";
	if ($_REQUEST[phone]==$i) $sqlstr1[] = "phone";
	if ($_REQUEST[fax]==$i) $sqlstr1[] = "fax";
	if ($_REQUEST[field1]==$i) $sqlstr1[] = "field1";
	if ($_REQUEST[field2]==$i) $sqlstr1[] = "field2";
	if ($_REQUEST[field3]==$i) $sqlstr1[] = "field3";
	if ($_REQUEST[field4]==$i) $sqlstr1[] = "field4";
	if ($_REQUEST[field5]==$i) $sqlstr1[] = "field5";
	$i++;
}
while (list($key, $val) = each($sqlstr1)) {
    $sqlstr .= "$val,";
}
$sqlstr = substr($sqlstr,0,strlen($sqlstr)-1);
//print $sqlstr;

if ($_REQUEST[serveremailfile]!="") {
	$emailfile = $serveremailfile;
} else {
	$emailfile = $_FILES['emailfile']['tmp_name'];
}
//$string = trim($emails);
$binary_junk=addslashes(fread(fopen($emailfile, "r"), filesize($emailfile)));
$line = strtok($binary_junk,"\n");
$duplicates = 0;
$added = 0;
$invalid = 0;
$log = "";
$c=1;
include "checkemail.php";
ob_end_flush();
ob_clean();
flush();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Insert Emails</title>
</head>

<body>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
<?php
print "......................................... \n<br>";
print "We are now processing the incoming file.... \n<br>";
print "......................................... \n<br>";
flush();
	while ($line) {
		$line = trim($line);
		$myvalues = split($terminated,$line);
		if (count($myvalues)!=count($sqlstr1)) {
			print "Data does not match. Invalid. Try again.";
			exit;
		}
		$j=0; $sqlstr2 = "";
		while ($j<count($myvalues)) {
			$sqlstr2 .= "'$myvalues[$j]',";
			$j++;
		}
		//print $sqlstr2."<br>";
		$email = $_REQUEST[email];
		$emailaddress = $myvalues[$email-1];
		//print $emailaddress."<br>";
		//print "$c. $line<br>";
		if (!check_email($emailaddress)) { $log .="INVALID: $emailaddress<br>"; $invalid++; }

		else if (sql_exist("select email from $tableMail where email='$emailaddress' and groupid='$groupid'") && $duplicate) { 
			if ($overwrite) {
				$log .= "UPDATED: $emailaddress<br>"; 
			} else {
				$log .= "DUPLICATE: $emailaddress<br>"; 
			}
			$duplicates++;
			if ($overwrite) {
				// update that line
				$SQL = "UPDATE $tableMail set ";
				$j=0; $sqlstring = "";
				while ($j<count($myvalues)) {
					$sqlstring .= "$sqlstr1[$j]='$myvalues[$j]',";
					$j++;
				}
				$SQL .= substr($sqlstring,0,strlen($sqlstring)-1);
				$SQL .= " WHERE email='$emailaddress' and groupid='$groupid'";
				sql_query($SQL);
			}
		}
		else { 
			// insert into
			$SQL = "INSERT INTO $tableMail ($sqlstr,groupid) VALUES ";
			$j=0; $sqlstring = "";
			while ($j<count($myvalues)) {
				$sqlstring .= "'$myvalues[$j]',";
				$j++;
			}
			$SQL .= "($sqlstring'$groupid')";
			//print "INSERT: $SQL<br>";
			sql_query($SQL); $added++; 
		}
		$line = strtok("\n");
		if ($c%10==0) {
			print ".";
		}
		if ($c%100==0) {
			print " $c processed | new: $added | duplicate: $duplicates | invalid: $invalid | <br>\n";
		}
		$c++;
		flush();
		ob_flush(); 
	}
	include "processcomplete.php";
	process_complete($invalid, $duplicates, $added, $groupid, $log);
	include "footer.php";
	ob_end_flush();
exit;
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Insert Emails and other data</title>
</head>

<body>
<font face=arial size=2>
<form action=importfromcsv.php method=post ENCTYPE="multipart/form-data">
<input type=hidden name=add value="Upload">
<table border=0 bgcolor=#DCDCDC width=75% align=center>
<tr><td colspan=2 align="center"><font face=arial size=2><strong>Import New Member Data from CSV/Text File</strong></font></td></tr>
<tr><td colspan=2><font face=arial size=2><strong>Fields Order</strong></font></td></tr>
<tr><td colspan=2><font face=arial size=1>Type 1, 2, 3, ... numbers in front of each field in the order of the data present in csv file. Make sure that only those fields have numbers which are present in the csv file, leave the rest empty. If it does not match, the import will not work and may cause wrong data to be entered.</font></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Email Address:</strong></font></td><td><input type=text name="email" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Title:</strong></font></td><td><input type=text name="title" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Full Name:</strong></font></td><td><input type=text name="name" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Address Line 1:</strong></font></td><td><input type=text name="address1" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Address Line 2:</strong></font></td><td><input type=text name="address2" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>City:</strong></font></td><td><input type=text name="city" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>State:</strong></font></td><td><input type=text name="state" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Zip/Postal Code:</strong></font></td><td><input type=text name="zip" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Country:</strong></font></td><td><input type=text name="country" value="" size=3></td></tr>
<tr><td colspan=2><font face=arial size=2><strong>Company Info</strong></font></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Company:</strong></font></td><td><input type=text name="company" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Job Title:</strong></font></td><td><input type=text name="job" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Web URL:</strong></font></td><td><input type=text name="url" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Telephone:</strong></font></td><td><input type=text name="phone" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Fax:</strong></font></td><td><input type=text name="fax" value="" size=3></td></tr>
<tr><td colspan=2><font face=arial size=2><strong>Additional Fields</strong></font></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 1:</strong></font></td><td><input type=text name="field1" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 2:</strong></font></td><td><input type=text name="field2" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 3:</strong></font></td><td><input type=text name="field3" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 4:</strong></font></td><td><input type=text name="field4" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 5:</strong></font></td><td><input type=text name="field5" value="" size=3></td></tr>
<tr><td colspan=2><hr size="1"></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Fields Terminated by:</strong></font></td><td><input type=text name="terminated" value="" size=3> <font face="Courier New, Courier, mono" size="2">( e.g. ; )</td></tr>
<tr><td align="right"><font face="arial" size="2"><a href="javascript:openWindow('help.php?id=16')">?</a></font> 
<input type="checkbox" name="duplicate" value="1" checked></td>
<td><font face="arial" size="2">Filter duplicate email addresses</font></td></tr>
<tr><td align="right"><font face="arial" size="2"><a href="javascript:openWindow('help.php?id=17')">?</a></font> 
<input type="checkbox" name="overwrite" value="1" checked></td>
<td><font face="arial" size="2">Overwrite Duplicate</font></td></tr>

<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptions("select id,name from $tableGroup order by name");
?>
</select>
</td>
</tr>
<tr><td colspan=2><hr size="1"></td></tr>
<tr><td align=right><font face=arial size=2>Upload CSV File:</font></td>
<td><input type="file" name="emailfile"></td></tr>
<tr><td colspan=2 align="center"><font face="Arial, Helvetica, sans-serif" size="2"><strong>OR</strong></font></td></tr>
<tr><td align=right><font face=arial size=2>CSV File on Server:</font></td>
<td><input type="text" name="serveremailfile" size="50"></td></tr>

<tr><td colspan=2><hr size="1"></td></tr>
<tr><td colspan=2 align=right><input type="submit" name="add" value="Upload"></td></tr>

</table>
</form>
<p>

<?php include "footer.php"; ?>
</font>
</body>
</html>
