<?php
// Bulk Mailer X (BMX)
// February, 2005. Release 3.1.
// Copyright (c) 1997-2005 Webx Networks (http://www.webx.net)


include "mysql.php";

if (!sql_exist("select id,name from $tableGroup order by name")) {
	print "No groups defined. You need a group to add email addresses. Please login in the backend admin and add a group.\n\n";
	exit;
}
print "\n\n";
print "**************************************\n";
print "Welcome to Bulk Mailer X Import Engine\n";
print "Copyright (c) Webx Networks 1997-2005.\n";
print "**************************************\n\n";
print "Groups Available in the System:\n";
	$result = mysql_query("select id,name from $tableGroup order by name");
	sql_error_check($result, $sql);
	while ($row = mysql_fetch_array($result)) {
	    print "[$row[0]] $row[1]\n";
	}

print "Enter Group ID [?] (where you want to import email addresses): ";
$stdin = fopen("php://stdin", 'r'); 
while(($data = fgetc($stdin)) != "\n") {
	$groupid .= $data;
}
$groupid = $groupid/1;
if ($groupid==0) {
	print "Invalid group. You need to select a group to add email addresses. Try again!\n\n";
	exit;
}

$DATA = sql_data("select name from $tableGroup where id='$groupid'");
$groupName = $DATA[0];
print "\nYou have selected \"$groupName\"\n\n";

print "Enter file name including full path on this server (containing email addresses one per line in clear text)\n";
print "File Name: ";

while(($data = fgetc($stdin)) != "\n") {
	$emailfile .= $data;
}
	if ( !is_file($emailfile) ) {
		print "File \"$emailfile\" does not exist or not readable. Please enter a valid file name and try again!\n\n";
		exit;
	}

print "\nDo you want to filter duplicates (choose yes if the data file is not clean and may contain duplicate email addresses)\n";
print "If you have large data to import, its a good idea to disable filter duplicate as it will speed up the import process.\n";
print "Filter Duplicates? [Y/n]: ";

while(($data = fgetc($stdin)) != "\n") {
	$duplicate .= $data;
}

if ($duplicate=="n" or $duplicate=="N" or $duplicate=="no" or $duplicate=="NO") {
	$duplicate=0;
	print "\nNot checking for duplicates...\n";
} else {
	$duplicate=1;
	print "\nChecking for duplicates while ";
}

print "importing \"$emailfile\" ...\n\n";

//$string = trim($emails);
$binary_junk=addslashes(fread(fopen($emailfile, "r"), filesize($emailfile)));
$line = strtok($binary_junk,"\n");
$duplicates = 0;
$added = 0;
$invalid = 0;
$log = "";
$c=1;
include "checkemail.php";
ob_end_flush();
ob_clean();
flush();

print "......................................... \n";
print "We are now processing the incoming file.... \n";
print "......................................... \n";
flush();
	while ($line) {
		$line = trim($line);
		//print "$c. $line<br>";
		if (!check_email($line)) { $log .="INVALID: $line<br>"; $invalid++; }
		else if ($duplicate) {
			if (sql_exist("select email from $tableMail where email='$line' and groupid='$groupid'")) {
				$log .= "DUPLICATE: $line<br>"; 
				$duplicates++; 
			} else {
				sql_query("insert into $tableMail (email, groupid) values ('$line', $groupid)"); 
				$added++; 
			}
		} else {
			sql_query("insert into $tableMail (email, groupid) values ('$line', $groupid)"); 
			$added++; 
		}		
		$line = strtok("\n");
		if ($c%10==0) {
			print ".";
		}
		if ($c%100==0) {
			print " $c processed | new: $added | duplicate: $duplicates | invalid: $invalid | \n";
		}
		$c++;
		flush();
		ob_flush(); 
	}
	print "Processing complete.\n\n";
	print "**************************************\n";
	print "Group: $groupName\n";
	print "Processed: $c\n";
	print "Invalid: $invalid\n";
	print "Duplicates: $invalid\n";
	print "New Added: $added\n";
	print "Bulk Mailer X - Copyright (c) Webx Networks 1997-2005.\n";
	print "******************************************************\n";
	//include "processcomplete.php";
	//process_complete($invalid, $duplicates, $added, $groupid, $log);
	//include "footer.php";
	ob_end_flush();
exit;
?>
