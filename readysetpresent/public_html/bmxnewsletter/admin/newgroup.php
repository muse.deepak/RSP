<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

$name = $_POST[name];
$create = $_POST[create];

if ($name!="" and $create=="Create") {
	$name = addslashes($name);
	$from_name = addslashes($_POST[from_name]);
	$from_email = addslashes($_POST[from_email]);
	$sql = "insert into $tableGroup (name,from_name,from_email) values ('$name','$from_name','$from_email')";
	sql_query($sql);
	$microtime = time();
	Header("Location: groups.php?refresh=$microtime");
	exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Groups: New Group</title>
</head>

<body>

<font face=arial size=2>
<form action="newgroup.php" method="post">
<input type="hidden" name="create" value="Create">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Create New Group</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Name of Group:</font></td>
<td><input name=name value="" size=30 maxlength=100></td>
</tr>
<tr>
<td align=right><font face=arial size=2>From Name:</font></td>
<td><input name=from_name value="" size=30 maxlength=50> 
<font face=arial size=2>Used for sending subscription/unsubscription emails,</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>From Email:</font></td>
<td><input name=from_email value="" size=30 maxlength=60>
<font face=arial size=2>for this particular group.</font>
</td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="create" value="Create"></td></tr>
</table>
</form>
<blockquote>
<font face=arial size=2>
<strong>Tips:</strong><br>
Name of the group is a unique name that identifies your mailing list.<br>
'From Name' and 'From Email' is used in the 'From:' address of (un)subscription verification emails for this particular group. So that if a user replys to those verification emails, they are returned to group owner/webmaster. You can set the 'From Email' to an invalid address if you do not want a reply from verification emails.
<p>
</blockquote>
<?php include "footer.php"; ?>