<?php

$configList = "adminUserName,adminPassword";
include "mysql.php";
session_start();

$admin = $_POST[admin];
$password = $_POST[password];

if ($admin=="")
{
	$loginError = "Please enter login";
	$_SESSION['loginError'] = $loginError;
	$_SESSION['admin'] = $admin;
	$_SESSION['password'] = $password;
	Header("Location: login.php");
	exit;
}

if ($admin!=$adminUserName) 
{
	$loginError = "No such user!";
	$_SESSION['loginError'] = $loginError;
	$_SESSION['admin'] = $admin;
	$_SESSION['password'] = $password;
	Header("Location: login.php");
	exit;
}

if ($password=="")
{
	$passwordError = "Please specify your password!";
	$_SESSION['passwordError'] = $passwordError;
	$_SESSION['admin'] = $admin;
	$_SESSION['password'] = $password;
	Header("Location: login.php");
	exit;
}

if ($admin==$adminUserName && $password==$adminPassword) { $adminAllowed=1; } else { $adminAllowed=0; }

if ($adminAllowed==0)
{
	$passwordError = "Wrong Password!";
	$_SESSION['passwordError'] = $passwordError;
	$_SESSION['admin'] = $admin;
	$_SESSION['password'] = $password;
	Header("Location: login.php");
	exit;
}

$_SESSION['adminAllowed'] = $adminAllowed;

{
	Header("Location: index.php");
}
?>