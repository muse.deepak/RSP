<?php

include "checksession.php";
$configList = "adminPassword";
include "mysql.php";
$passRequired = $adminPassword;

$password1 = trim($_POST[password1]);
$password2 = $_POST[password2];
$oldPass = $_POST[oldPass];

if ($oldPass=="")
{
	$oldError = "Please enter old password";
	session_register("oldError");
	Header("Location: passwd.php");
	exit;
}
if ($oldPass!=$passRequired)
{
	$oldError = "Old Password wrong";
	session_register("oldError");
	Header("Location: passwd.php");
	exit;
}

if ($password1=="")
{
	$passwordError1 = "Please specify your new password!";
	session_register("passwordError1");
	Header("Location: passwd.php");
	exit;
}

if ($password2=="")
{
	$passwordError2 = "Please confirm your new password!";
	session_register("passwordError2");
	Header("Location: passwd.php");
	exit;
}

if ($password1!=$password2)
{
	$passwordError2 = "Password mismatch!";
	session_register("passwordError2");
	Header("Location: passwd.php");
	exit;
}

if ($oldPass==$passRequired && $password1==$password2) 
{
	$password1 = addslashes($password1);
	sql_query("update $tableConfig set value='$password1' where keyname='adminPassword'");
	?>
<html><body>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
  <!--
  window.alert("Password changed successfully.");
  location.href='index.php';
  //-->
  </SCRIPT>
<div align="center">
<font face=arial size=2>
Password changed successfully. <a href=index.php>Click here to continue...</a>
</font>
</div>
</body></html>	

	
	<?php
	exit;
}
?>