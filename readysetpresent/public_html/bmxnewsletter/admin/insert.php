<?php
// Bulk Mailer X (BMX)
// January, 2005. Release 3.1.
// Copyright (c) 1997-2005 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

if (!sql_exist("select id,name from $tableGroup order by name")) {
	print "<font face=arial size=2>No groups defined. You need a group to add email addresses. <a href=groups.php>Click here to add one</a>.</font>";
	exit;
}
$add = $_POST[add];
if ($add=="Add") {

$groupid = $_POST[groupid]/1;
if ($groupid==0) {
	print "<font face=arial size=2>No group selected. You need to select a group to add email addresses. <a href=insert.php>Click here to try again</a>.</font>";
	exit;
}

$string = trim($_POST[emails]);
$line = strtok($string,"\n");
$duplicates = 0;
$added = 0;
$invalid = 0;
$log ="";
$c=1;
include "checkemail.php";
ob_end_flush();
ob_clean();
flush();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Insert Emails</title>
</head>

<body>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
<?php
print "......................................... \n<br>";
print "We are now processing the incoming data.... \n<br>";
print "......................................... \n<br>";
flush();
	while ($line) {
		$line = trim($line);
		//print "$c. $line<br>";
		if (!check_email($line)) { $log .="INVALID: $line<br>"; $invalid++; }
		else if ($_POST[duplicate]) {
			if (sql_exist("select email from $tableMail where email='$line' and groupid='$groupid'")) {
				$log .= "DUPLICATE: $line<br>"; 
				$duplicates++; 
			} else {
				sql_query("insert into $tableMail (email, groupid) values ('$line', $groupid)"); 
				$added++; 
			}
		} else {
			sql_query("insert into $tableMail (email, groupid) values ('$line', $groupid)"); 
			$added++; 
		}		
		$line = strtok("\n");
		if ($c%10==0) {
			print ".";
		}
		if ($c%100==0) {
			print " $c processed | new: $added | duplicate: $duplicates | invalid: $invalid | <br>\n";
		}
		$c++;
		flush();
		ob_flush(); 
	}

	include "processcomplete.php";
	process_complete($invalid, $duplicates, $added, $groupid, $log);
	include "footer.php";
	ob_end_flush();
exit;
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Insert Emails</title>
</head>

<body>
<font face=arial size=2>
<form action="insert.php" method="post">
<input type="hidden" name="add" value="Add">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Add Email Addresses</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptions("select id,name from $tableGroup order by name");
?>
</select>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Enter Email Addresses:<br>(one per line)</font></td>
<td>
<textarea cols="60" rows="5" name="emails"></textarea>
</td>
</tr>
<tr><td align="right"><font face=arial size=2><a href="javascript:openWindow('help.php?id=16')">?</a></font> 
<input type="checkbox" name="duplicate" value="1" checked></td>
<td><font face=arial size=2>Filter duplicate email addresses</font></td></tr>
<tr><td colspan=2 align=right><input type="submit" name="add" value="Add"></td></tr>
</table>
</form>
<p>

<?php include "footer.php"; ?>
</font>
</body>
</html>
