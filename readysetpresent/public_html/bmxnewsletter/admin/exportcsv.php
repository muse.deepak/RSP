<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

	include "checksession.php";
	include "mysql.php";

$action = $_POST[action];

if ($action=="export") {

	$groupid = $_POST[groupid]/1;
	if ($groupid==0) { Header("Location: groups.php"); exit; }
	$data = sql_data("select name from $tableGroup where id=$groupid");
	$groupname = $data[0];
	$terminated = $_POST[terminated];
	$buffer = "";
	$result = sql_query("select * from $tableMail where groupid=$groupid order by email");

$i=1;
while ($i<20) {
	if ($_REQUEST[email]==$i) $sqlstr1[] = "email"; 
	if ($_REQUEST[title]==$i) $sqlstr1[] = "title";
	if ($_REQUEST[name]==$i) $sqlstr1[] = "name";
	if ($_REQUEST[address1]==$i) $sqlstr1[] = "address1";
	if ($_REQUEST[address2]==$i) $sqlstr1[] = "address2";
	if ($_REQUEST[city]==$i) $sqlstr1[] = "city";
	if ($_REQUEST[state]==$i) $sqlstr1[] = "state";
	if ($_REQUEST[zip]==$i) $sqlstr1[] = "zip";
	if ($_REQUEST[country]==$i) $sqlstr1[] = "country";
	if ($_REQUEST[company]==$i) $sqlstr1[] = "company";
	if ($_REQUEST[job]==$i) $sqlstr1[] = "job";
	if ($_REQUEST[url]==$i) $sqlstr1[] = "url";
	if ($_REQUEST[phone]==$i) $sqlstr1[] = "phone";
	if ($_REQUEST[fax]==$i) $sqlstr1[] = "fax";
	if ($_REQUEST[field1]==$i) $sqlstr1[] = "field1";
	if ($_REQUEST[field2]==$i) $sqlstr1[] = "field2";
	if ($_REQUEST[field3]==$i) $sqlstr1[] = "field3";
	if ($_REQUEST[field4]==$i) $sqlstr1[] = "field4";
	if ($_REQUEST[field5]==$i) $sqlstr1[] = "field5";
	$i++;
}

	while ($row = mysql_fetch_array($result)) {
		$bufferstr = "";
		$sqlstr = $sqlstr1;
		while (list($key, $val) = each($sqlstr)) {
	    	$bufferstr .= $row[$val].$terminated;
		}
		$bufferstr = substr($bufferstr,0,strlen($bufferstr)-1);
		$buffer .= "$bufferstr\r\n";
	}

	header("Cache-control: max-age=31536000");
	header("Expires: " . gmdate("D, d M Y H:i:s",time()) . "GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s",time()) . "GMT");
	header("Content-disposition: attachment; filename=$groupname.txt");
	header("Content-Length: ".strlen($buffer));
	header("Content-type: text");
	print $buffer;
	exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Export CSV data</title>
</head>

<body>
<font face=arial size=2>

<font face=arial size=2>
<form action=exportcsv.php method=post>
<input type=hidden name=action value="export">
<table border=0 bgcolor=#DCDCDC width=75% align=center>
<tr><td colspan=2 align="center"><font face=arial size=2><strong>Export Member Data as csv/text File</strong></font></td></tr>
<tr><td colspan=2><font face=arial size=2><strong>Fields Order</strong></font></td></tr>
<tr><td colspan=2><font face=arial size=1>Type 1, 2, 3, ... numbers in front of each field in the order of the data you want to export. Leave the fields empty if you do not want to export them.</font></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Email Address:</strong></font></td><td><input type=text name="email" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Title:</strong></font></td><td><input type=text name="title" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Full Name:</strong></font></td><td><input type=text name="name" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Address Line 1:</strong></font></td><td><input type=text name="address1" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Address Line 2:</strong></font></td><td><input type=text name="address2" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>City:</strong></font></td><td><input type=text name="city" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>State:</strong></font></td><td><input type=text name="state" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Zip/Postal Code:</strong></font></td><td><input type=text name="zip" value="" size=3></td></tr>

<tr><td align="right"><font face=arial size=2><strong>Country:</strong></font></td><td><input type=text name="country" value="" size=3></td></tr>
<tr><td colspan=2><font face=arial size=2><strong>Company Info</strong></font></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Company:</strong></font></td><td><input type=text name="company" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Job Title:</strong></font></td><td><input type=text name="job" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Web URL:</strong></font></td><td><input type=text name="url" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Telephone:</strong></font></td><td><input type=text name="phone" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Fax:</strong></font></td><td><input type=text name="fax" value="" size=3></td></tr>
<tr><td colspan=2><font face=arial size=2><strong>Additional Fields</strong></font></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 1:</strong></font></td><td><input type=text name="field1" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 2:</strong></font></td><td><input type=text name="field2" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 3:</strong></font></td><td><input type=text name="field3" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 4:</strong></font></td><td><input type=text name="field4" value="" size=3></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Field 5:</strong></font></td><td><input type=text name="field5" value="" size=3></td></tr>
<tr><td colspan=2><hr size="1"></td></tr>
<tr><td align="right"><font face=arial size=2><strong>Fields Terminated by:</strong></font></td><td><input type=text name="terminated" value="" size=3> <font face=arial size=1>( e.g. ; )</td></tr>
<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptions("select id,name from $tableGroup order by name");
?>
</select>
</td>
</tr>
<tr><td colspan=2><hr size="1"></td></tr>
<tr><td colspan=2 align=right><input type="submit" name="do" value="Export"></td></tr>

</table>
</form>
<p>

<?php include "footer.php";?>
</font>
</body>
</html>
