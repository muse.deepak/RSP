<?php
// Bulk Mailer X (BMX)
// March, 2004. Release 3a.
// Copyright (c) 1997-2004 Webx Networks (http://www.webx.net)
// insert into real_table (col1, col2) select x, y from temp_table;

include "checksession.php";
include "mysql.php";

$merge = $_POST[merge];
$groupid1 = $_POST[groupid1];
$groupid2 = $_POST[groupid2];

if ($merge=="Merge") {
	if (($groupid1==0) || ($groupid2==0) || (($groupid1==$groupid2))) {
		print "<font face=arial size=2>Two separate groups not selected. Try again by clicking <a href=merge.php>here</a>.";
		include "footer.php";
		exit;
	}

	$sql = "select distinct t1.email from $tableMail t2, $tableMail t1 where (t1.email<>t2.email and t1.groupid='$groupid1' and t2.groupid='$groupid2');";
	
	$Result = sql_query($sql);
	$num_rows = mysql_num_rows($Result);
	$row=0;
	if ($num_rows>1) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Merge Result</title>
</head>

<body>
<font face=arial size=2>

<?php
		while($row < $num_rows)
	    {
			$DATA = mysql_fetch_array($Result);
			print $DATA[0]."<br>";
			$row++;
		}
		include "footer.php"; exit; 
	} else {
	?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Merge Result</title>
</head>

<body>
<font face=arial size=2>
No new email address found. <a href="merge.php">Click here</a> to try again.
<?php include "footer.php"; exit; }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Merge Groups</title>
</head>

<body>

<form action=merge.php method=post>
<input type=hidden name=merge value="Merge">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2 align="center"><font face=arial size=2><strong>Merge Email Addresses</strong><br>* copy email addresses from first group into second group *</font></td></tr>
<tr>
<td align=right><font face=arial size=2>Group from:</font></td>
<td>
<select name="groupid1">
<option value=0>-- Select a Group --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptions($sql);
?>
</select>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Group to:</font></td>
<td>
<select name="groupid2">
<option value=0>-- Select a Group --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptions($sql);
?>
</select>
</td>
</tr>
<tr><td colspan=2 align=right><input type="submit" name="merge" value="Merge"></td></tr>
</table>
</form>
<blockquote>
<font face=arial size=2>
<strong>Steps:</strong><br>
If you want to merge email addresses into a new group, first create an empty group from <a href="newgroup.php">here</a>. And then merge addresses from required group(s) into that newly created group using this Merge feature. This will copy all data alongwith the email addresses including subscription status, address etc.<p>
</blockquote>

<?php include "footer.php"; ?>
</body>
</html>
