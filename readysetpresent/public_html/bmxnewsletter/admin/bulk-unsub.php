<?php
// Bulk Mailer X (BMX)
// July, 2007. Release 3.2.
// Copyright (c) 1997-2007 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

if (!sql_exist("select id,name from $tableGroup order by name")) {
	print "<font face=arial size=2>No groups defined. You need a group to unsubscribe email addresses. <a href=groups.php>Click here to add one</a>.</font>";
	exit;
}

$unsub = $_POST[unsub];

if ($unsub=="Unsubscribe") {

$groupid = $_POST[groupid]/1;
if ($groupid==0) {
	print "<font face=arial size=2>No group selected. You need to select a group to unsubscribe email addresses from. <a href=insert.php>Click here to try again</a>.</font>";
	exit;
}

$string = trim($_POST[emails]);
$line = strtok($string,"\n");
$duplicates = 0;
$added = 0;
$invalid = 0;
$log ="";
$c=1;
include "checkemail.php";
ob_end_flush();
ob_clean();
flush();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Bulk Unsubscribe</title>
</head>

<body>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
<?php
print "......................................... \n<br>";
print "We are now processing the incoming data.... \n<br>";
print "......................................... \n<br>";
flush();
	while ($line) {
		$line = trim($line);
		//print "$c. $line<br>";
		if (!check_email($line)) { $log .="INVALID: $line<br>"; $invalid++; }
		else if (!sql_exist("select email from $tableMail where email='$line' and groupid='$groupid'")) { $log .= "NOT FOUND: $line<br>"; }
		else { 
			sql_query("update $tableMail set subscription='0' where email='$line' and groupid='$groupid'"); 
			$duplicates++;
		}
		$line = strtok("\n");
		if ($c%10==0) {
			print ".";
		}
		if ($c%100==0) {
			print " $c processed | unsubscribed: $duplicates<br>\n";
		}
		$c++;
		flush();
		ob_flush(); 
	}

	include "processcomplete.php";
	process_complete($invalid, $duplicates, $added, $groupid, $log);
	include "footer.php";
	ob_end_flush();
exit;
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Unsubscribe Emails</title>
</head>

<body>
<font face=arial size=2>
<form action="bulk-unsub.php" method="post">
<input type="hidden" name="unsub" value="Unsubscribe">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2 color="#CC0000"><strong>Unsubscribe Email Addresses</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptions("select id,name from $tableGroup order by name");
?>
</select>
</td>
</tr>
<td align=right><font face=arial size=2>Enter Email Addresses:<br>(one per line)</font></td>
<td>
<textarea cols="60" rows="5" name="emails"></textarea>
</td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="unsub" value="Unsubscribe"></td></tr>
</table>
</form>
<p>

<?php include "footer.php";?>
</font>
</body>
</html>
