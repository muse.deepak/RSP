<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

	$result = sql_query("select id,comment,date from $tableLog order by date");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX : Browse Logs</title>
</head>
<body>
<font face=arial size=2>

<div align=center><h2><em>Logs</em></h2>
<h4>These logs are just for debugging. You may delete them.</h4>
</div>
<table border=0 width=75% align=center>
<?php 
$c = 0;
while ($row = mysql_fetch_array($result)) { ?>
<tr>
<td><font face=arial size=2><a href=checklog.php?id=<?php print $row[id]?>><?php print $row[date].", ".$row[comment];?></a></font></td><td><font face=arial size=2>[<a href=deletelog.php?id=<?php print $row[id];?> onclick="javascript:return confirm('Are you sure you want to delete?');">delete</a>]</font></td>
</tr>
<?php $c++; } ?>
</table>
<?php if ($c<1) { 
		print "<div align=center>No logs available.</div>";
	} else {
		print "<div align=center><a href=deletelog.php>Delete All Logs</a></div>";
	}
?>
<p>
<?php include "footer.php";?>
</font>
</body>
</html>
