<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

if (!sql_exist("select id,name from $tableGroup order by name")) {
	print "<font face=arial size=2>No groups defined. You need a group to remove email addresses. <a href=groups.php>Click here to add one</a>.</font>";
	exit;
}

$remove = $_POST[remove];

if ($remove=="Remove") {

$groupid = $_POST[groupid]/1;
if ($groupid==0) {
	print "<font face=arial size=2>No group selected. You need to select a group to remove email addresses from. <a href=insert.php>Click here to try again</a>.</font>";
	exit;
}

$string = trim($_POST[emails]);
$line = strtok($string,"\n");
$duplicates = 0;
$added = 0;
$invalid = 0;
$log ="";
$c=0;
include "checkemail.php";
	while ($line) {
		$line = trim($line);
		//print "$c. $line<br>";
		if (!check_email($line)) { $log .="INVALID: $line<br>"; $invalid++; }
		else if (!sql_exist("select email from $tableMail where email='$line' and groupid='$groupid'")) { $log .= "NOT FOUND: $line<br>"; }
		else { 
			sql_query("delete from $tableMail where email='$line' and groupid='$groupid'"); 
			$duplicates++;
		}
		$line = strtok("\n");
		$c++;
	}

	include "processcomplete.php";
	process_complete($invalid, $duplicates, $added, $groupid, $log);
	include "footer.php";
exit;
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX : Remove Emails</title>
</head>

<body>
<font face=arial size=2>
<form action="remove.php" method="post">
<input type="hidden" name="remove" value="Remove">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2 color="#CC0000"><strong>Remove Email Addresses</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Select Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
DropDownOptions("select id,name from $tableGroup order by name");
?>
</select>
</td>
</tr>
<td align=right><font face=arial size=2>Enter Email Addresses:<br>(one per line)</font></td>
<td>
<textarea cols="60" rows="5" name="emails"></textarea>
</td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="remove" value="Remove"></td></tr>
</table>
</form>
<p>

<?php include "footer.php";?>
</font>
</body>
</html>
