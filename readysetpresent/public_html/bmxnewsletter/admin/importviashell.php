<?php include "checksession.php"; ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Bulk Mailer X : Import via Shell Guide</title>
</head>
<body>
<div align=center>
<font face="arial" size="2">
<h2>Bulk Mailer X : Import via Shell Guide</h2>
</div>
<blockquote>
You can import large data file from linux shell as well. This option is good if your web browser session times out due to php execution time limits. 
To use this import engine, you need to have php CLI binary available on system path. You can check this by issuing the following shell command on linux:
<pre style="font-size: 10pt">
$ <font color="#990000"><b>php -v</b></font>
</pre>
This will give an output something like this:

<pre style="font-size: 10pt">
PHP 4.3.10 (cli) (built: Dec 25 2004 15:50:05)
Copyright (c) 1997-2004 The PHP Group
Zend Engine v1.3.0, Copyright (c) 1998-2004 Zend Technologies
    with Zend Extension Manager v1.0.6, Copyright (c) 2003-2004, by Zend Technologies
    with Zend Optimizer v2.5.7, Copyright (c) 1998-2004, by Zend Technologies
</pre>

If all is ok. You can change directory to your BMX admin folder, e.g.:
<pre style="font-size: 10pt">
$ <font color="#990000"><b>cd ~/public_html/bmx/admin/</b></font>
</pre>
Once there, issue this command:
<pre style="font-size: 10pt; font-weight: bold">
$ <font color="#990000"><b>php -q import.php</b></font>
</pre>
It will ask you to select group id, and provide the data file name to import. Rest is straight forward process. If there is any problem, do not hesitate to contact us at support@webx.net
</blockquote>

<?php include "footer.php"; ?>

</font>

</body>
</html>
