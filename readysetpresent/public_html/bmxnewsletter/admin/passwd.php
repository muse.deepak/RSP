<?php 
include "checksession.php";
$configList = "adminUserName";
include "mysql.php";
$loginRequired = $adminUserName;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Change Admin Password</title>
<style type="text/css">
<!--
.warningsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-color: red;
	font-size: 10pt; 
	font-style: normal; 
	line-height: normal; 
	font-weight: normal; 
	text-align: justify;
	letter-spacing: normal;
	}	
-->
</style>

</head>

<body>
<form name="login" action="passwd-process.php" method="post">
<div align="center">
<font face=arial>
<h2>Change Your Admin Password</h2>
<table width="75%" border="0" bgcolor="#DCDCDC">
		<tr>
			<td align="right" valign="top" width="50%"><font size="2" face="arial"><b>Login:</b></font></td>
			<td valign="top" width="50%"><font size="2" face="arial"><?php print $loginRequired; ?></font>
			</td>
		</tr>

		<tr>
			<td align="right" valign="top" width="50%"><font size="2" face="arial"><b>Old Password:</b></font></td>
			<td valign="top" width="50%"><input type="password" size="20" maxlength="50" name="oldPass">
			<?php if ($_SESSION[oldError]) { print "<br><span class='warningsmall'>$_SESSION[oldError]</span>";}?>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top"><font size="2" face="arial"><b>New Password:</b></font></td>
			<td valign="top"><input type="password" size="20" maxlength="20" name="password1" value="<?php print $_SESSION[password]; ?>">
			<?php if ($_SESSION[passwordError1]) { print "<br><span class='warningsmall'>$_SESSION[passwordError1]</span>";}?>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top"><font size="2" face="arial"><b>New Password (confirm):</b></font></td>
			<td valign="top"><input type="password" size="20" maxlength="20" name="password2">
			<?php if ($_SESSION[passwordError2]) { print "<br><span class='warningsmall'>$_SESSION[passwordError2]</span>";}?>
			</td>
		</tr>

		<tr>
		<td colspan="2">
		<div align="right"><INPUT type="submit" value="Change Password" name="password"></div>
		</td>
		</tr>
	</table>
</div>
</font>
<?php
session_unregister("oldError");
session_unregister("passwordError1");
session_unregister("passwordError2");
session_unregister("password");
include "footer.php";
?>

