<?php include "checksession.php"; ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Bulk Mailer X : Public Subscription Guide</title>
</head>
<body>
<div align=center>
<font face=arial size=2>
<h2>Bulk Mailer X : Public Subscription Guide</h2>
</div>
<blockquote>
<strong>Configuration</strong><p>
<ul>
<li>The 'newsletter' directory contains all the files to manage subscription.
<li>Edit 'config.php' and set the four variables $db_host, $db_user, $db_pass, and $db for your MySQL database settings. 
This is same as in admin config.php file. Two separate files make it easy to install the 'newsletter' anywhere.</li>
<li>One important configuration is <strong>'newsletter' Installated Location (URL)</strong> which you can setup <a href=systemconfig.php?con=1>here</a>.
<li><strong>Optionally</strong> you can also set other configuration parameters <a href=systemconfig.php>here</a>, like email messages for subscription/unsubscription, and other error and validation messages.</li>
</ul>
<strong>Final Setup</strong><p></p>
<ul>
<li>You can use <a href=systemconfig.php?con=5>static HTML code from here</a> to plug-in to any of your current website, or
<li>Use your 'newsletter' folder for subscription out of the box. For that you need to setup one variable:
<ul>
<li>Open 'index.php' from 'newsletter' folder in your favourite text editor, and edit this line:</li><p>

<font color=orange>&lt;input type="hidden" name="group" value="<font color=red>1</font>"&gt;</font>
<p>
Replace that "1" with appropriate Group ID. The group id can be found with your group name <a href="groups.php">here</a>.
<li>Open the newsletter directory in your browser and start subscribing and unsubscribing :-)</li>
</ul>
</ul>
That's it. If there is any problem, contact <a href="mailto:support@webx.net?Subject=Bulk Mailer X">support@webx.net</a>.
</blockquote>

<?php include "footer.php"; ?>
</font>

</body>
</html>
