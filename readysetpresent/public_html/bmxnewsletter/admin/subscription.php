<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

$subscription = $_REQUEST[subscription];

if ($subscription!="") {

	$groupid = $_REQUEST[groupid]/1;
	if ($groupid==0) {
		print "<font face=arial size=2>Group not selected. Please select a group and <a href=subscription.php>try again</a>.<p>";
		include "footer.php";
		exit;
	}
	$email = addslashes($_REQUEST[email]);

	if (!sql_exist("select email from $tableMail where email='$email' and groupid=$groupid")) {
		print "<font face=arial size=2>No such email address found. Please add it through <a href=insert.php>Import</a> or <a href=subscription.php>click here</a> to try again.<p>";
		include "footer.php";
		exit;
	}

	if ($subscription=="Subscribe") {
		if (sql_exist("select email from $tableMail where email='$email' and subscription=1 and groupid=$groupid")) {
			print "<font face=arial size=2>Already subscribed. <a href=subscription.php>Click here</a> to try another.<p>";
			include "footer.php";
			exit;
		}

		$sql = "update $tableMail set subscription=1 where email='$email' and groupid=$groupid";
		sql_query($sql);
		print "<font face=arial size=2><strong>Subscribed Now</strong>. <a href=subscription.php>Click here</a> to continue.<p>";
		include "footer.php";
		exit;
	}

	if ($subscription=="Unsubscribe") {
		if (sql_exist("select email from $tableMail where email='$email' and subscription=0 and groupid=$groupid")) {
			print "<font face=arial size=2>Already unsubscribed. <a href=subscription.php>Click here</a> to try another.";
			include "footer.php";
			exit;
		}

		$sql = "update $tableMail set subscription=0 where email='$email' and groupid=$groupid";
		sql_query($sql);
		print "<font face=arial size=2><strong>Unsubscribed Now</strong>. <a href=subscription.php>Click here</a> to continue.<p>";
		include "footer.php";
		exit;
	}

}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX: Subscription</title>
</head>
<body>
<form action="subscription.php" method="post">
<input type="hidden" name="search" value="Search">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Subscribe / Unsubscribe</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select Group --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptions($sql);
?>
</select>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Email Address:</font></td>
<td><input name=email value="" size=50 maxlength=100></td>
</tr>
<tr><td colspan=2 align=center><input type="submit" name="subscription" value="Subscribe">&nbsp;&nbsp;<input type="submit" name="subscription" value="Unsubscribe"></td></tr>
</table>
</form>

<?php include "footer.php"; ?>
</body>
</html>
