<?php
// Bulk Mailer X (BMX)
// January, 2005. Release 3.1.
// Copyright (c) 1997-2005 Webx Networks (http://www.webx.net)

include "checksession.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX : Groups</title>
</head>

<body>
<div align=center>
<font face=arial size=2>
<strong>Browse Groups</strong>
<table border=0 width=90% bgcolor=#DCDCDC align=center>
<tr>
	<td><font face=arial size=2><strong>Group ID</strong></td>
	<td><font face=arial size=2><strong>Group Name</strong></td>
	<td><font face=arial size=2><strong>Members</strong></td>
	<td><font face=arial size=2><strong>Subscribed</strong></td>
	<td><font face=arial size=2><strong>Edit</strong></td>
	<td><font face=arial size=2><strong>Empty</strong></td>
	<td><font face=arial size=2><strong>Delete</strong></td>
	<td><font face=arial size=2><strong>Export Email Only</strong></td>
	<td><font face=arial size=2><strong>Export csv</strong></td>
	

</tr>

<?php
include "mysql.php";
$result = sql_query("select id,name from $tableGroup order by name");

while ($row = mysql_fetch_array($result)) {
	$grouptotal = sql_data("select count(id) from $tableMail where groupid=$row[id]");
	$subscribed = sql_data("select count(id) from $tableMail where groupid=$row[id] and subscription=1");
	print "<tr>";
	print "<td><font face=arial size=2>$row[id]</font></td>";
	print "<td><a href=browsegroup.php?id=$row[id]>$row[name]</a></li></td>";
	print "<td><font face=arial size=2>($grouptotal[0])</font></td>";
	print "<td><font face=arial size=2>($subscribed[0])</font></td>";
	print "<td><font face=arial size=2>[<a href=editgroup.php?id=$row[id]>edit</a>]</font></td>";
	print "<td><font face=arial size=2>[<a href=emptygroup.php?id=$row[id] onclick=\"javascript:return confirm('Are you sure you want to delete all email addresses from this group permanently?');\">empty</a>]</font></td>";
	print "<td><font face=arial size=2>[<a href=deletegroup.php?id=$row[id] onclick=\"javascript:return confirm('Are you sure you want to delete it permanently and all emails in this group?');\">delete</a>]</font></td>";
	print "<td><font face=arial size=2>[ <a href=export.php?id=$row[id]&t=all>All</a> | <a href=export.php?id=$row[id]&t=sub>Sub</a> | <a href=export.php?id=$row[id]&t=unsub>UnSub</a> ]</font></td>";
	print "<td><font face=arial size=2>[<a href=exportcsv.php>Export CSV</a>]</font></td></tr>";
}
?>
</table>
</div>
<p>

</font>

<blockquote>
<font face=arial size=2>
<strong>Tips:</strong><br>
Click the name of the group to browse emails. You can then edit/delete individual emails.<br>
Edit: You can edit the name of the group.<br>
Empty: Removes all email addresses from that group.<br>
Delete: Deletes the group and all email addresses it contains.<br>
Export: Save all email addresses (one per line) in text file.
<p>
</blockquote>

<?php include "footer.php";?>

</body>
</html>
