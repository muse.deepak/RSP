<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

	include "checksession.php";
	include "mysql.php";
	$id = $_REQUEST[id]/1;
	if ($id==0) { Header("Location: groups.php"); exit; }
	$update = $_POST[update];
if ($update=="Update") {
	$name = addslashes($_POST[name]);
	$from_name = addslashes($_POST[from_name]);
	$from_email = addslashes($_POST[from_email]);
	if (strlen($name)<1) { 
		print "<font face=arial size=2>Name of group not valid. <a href=editgroup.php?id=$id>Click here</a> to try again.</font>"; 
		exit; 
	}
	sql_query("update $tableGroup set name='$name', from_name='$from_name', from_email='$from_email' where id=$id");
	$microtime = time();
	Header("Location: groups.php?refresh=$microtime");

}
else 
{
	$data = sql_data("select name,from_name,from_email from $tableGroup where id=$id");
	$name = $data[0];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX : Edit Group: <?php print $name; ?></title>
</head>

<body>
<div align=center>
<font face=arial size=2>

<form action=editgroup.php method=post>
<input type=hidden name=id value="<?php print $id;?>">
<input type=hidden name=update value="Update">
<table border=0 bgcolor=#DCDCDC>
<tr><td colspan=2><font face=arial size=2><strong>Edit Group</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Name of Group:</font></td>
<td><input name=name value="<?php print $name;?>" size=30 maxlength=100></td>
</tr>
<tr>
<td align=right><font face=arial size=2>From Name:</font></td>
<td><input name=from_name value="<?php print $data[from_name];?>" size=30 maxlength=50> 
<font face=arial size=2>Used for sending subscription/unsubscription emails,</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>From Email:</font></td>
<td><input name=from_email value="<?php print $data[from_email];?>" size=30 maxlength=60>
<font face=arial size=2>for this particular group.</font>
</td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="update" value="Update"></td></tr>
</table>
</form>
<p>

</font>
</div>
</body>
</html>
<?php 
include "footer.php";
} ?>