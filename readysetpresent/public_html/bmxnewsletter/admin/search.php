<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

include "checksession.php";
include "mysql.php";

$search = $_REQUEST[search];

if ($search=="Search") {
	$email = addslashes($_POST[email]);
	$groupid = $_POST[groupid]/1;
	$sql = "select id, email, groupid, subscription from $tableMail where email='$email'";
	if ($groupid!=0) $sql .= " and groupid=$groupid";
	$data = sql_data($sql);
	$id = $data[id];
	$email = $data[email];
	$groupid = $data[groupid];
	$subscription = $data[subscription];
	?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Search Result</title>
</head>

<body>
<font face=arial size=2>
<?php
	if ($email=="") {
?>
No email address found. <a href="search.php">Click here</a> to try again.
<?php include "footer.php"; exit; } ?>

<form action="editemail.php" method="post">
<input type="hidden" name="id" value="<?php print $id;?>">
<input type="hidden" name="update" value="Update">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Email Result</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptionsSelect($sql, $groupid);
?>
</select>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Email Address:</font></td>
<td><input name=email value="<?php print $email;?>" size=50 maxlength=100></td>
</tr>
<tr><td align=right><font face=arial size=2>Subscription:</font></td>
<td>
        <input type="radio" name="subscription" value="1" <?php if ($subscription==1) print "checked";?>>
        <font size="2" face="Arial, Helvetica, sans-serif">Yes</font> 
        <input type="radio" name="subscription" value="0" <?php if ($subscription==0) print "checked";?>>
        <font size="2" face="Arial, Helvetica, sans-serif"> No </font>

</td>
</tr>
<tr><td><font face=arial size=2>[<a href=deleteemail.php?id=<?php print $id;?> onClick="javascript:return confirm('Are you sure you want to delete it permanently?\n\n<?php print $email;?>');">delete</a>] [<a href=editemail.php?id=<?php print $id;?>');">edit</a>]</font></td><td align=right><input type="submit" name="update" value="Update"></td></tr>
</table>
</form>
<p>

</font>
<?php include "footer.php"; ?>
</body>
</html>
<?php exit; } ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>BMX: Search</title>
</head>

<body>

<form action="search.php" method="post">
<input type="hidden" name="search" value="Search">
<table border=0 bgcolor=#DCDCDC align=center>
<tr><td colspan=2><font face=arial size=2><strong>Search Email Address</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- All Groups --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptions($sql);
?>
</select>
</td>
</tr>
<tr>
<td align=right><font face=arial size=2>Email Address:</font></td>
<td><input name=email value="" size=50 maxlength=100></td>
</tr>

<tr><td colspan=2 align=right><input type="submit" name="search" value="Search"></td></tr>
</table>
</form>

<?php include "footer.php"; ?>
</body>
</html>
