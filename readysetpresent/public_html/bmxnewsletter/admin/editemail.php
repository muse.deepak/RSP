<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

	include "checksession.php";
	include "mysql.php";
	$id = $_REQUEST[id]/1;
	if ($id==0) { Header("Location: index.php"); exit; }

	$update = $_POST[update];
	
if ($update=="Update") {
	include "checkemail.php";
	$email = trim($_POST[email]);
	$groupid = $_POST[groupid]/1;
	if (check_email($email) && $groupid>0) {
	$subscription = addslashes($_POST[subscription]);
	$title = addslashes($_POST[title]);
	$name = addslashes($_POST[name]);
	$address1 = addslashes($_POST[address1]);
	$address2 = addslashes($_POST[address2]);
	$city = addslashes($_POST[city]);
	$state = addslashes($_POST[state]);
	$zip = addslashes($_POST[zip]);
	$country = addslashes($_POST[country]);
	$company = addslashes($_POST[company]);
	$job = addslashes($_POST[job]);
	$url = addslashes($_POST[url]);
	$phone = addslashes($_POST[phone]);
	$fax = addslashes($_POST[fax]);
	$field1 = addslashes($_POST[field1]);
	$field2 = addslashes($_POST[field2]);
	$field3 = addslashes($_POST[field3]);
	$field4 = addslashes($_POST[field4]);
	$field5 = addslashes($_POST[field5]);
	
	sql_query("update $tableMail set email='$email', groupid=$groupid, subscription='$subscription', title='$title', name='$name', address1='$address1', address2='$address2', city='$city', state='$state', zip='$zip', country='$country', company='$company', job='$job', url='$url', phone='$phone', fax='$fax', field1='$field1', field2='$field2', field3='$field3', field4='$field4', field5='$field5' where id=$id");
	$microtime = time();
	Header("Location: browsegroup.php?id=$groupid&refresh=$microtime");
	}
	else
	{ 
		print "<font face=arial size=2>Email address or group not valid. <a href=editemail.php?id=$id>Click here</a> to try again.</font>"; 
		exit;
	}

}
else 
{
	$data = sql_data("select * from $tableMail where id=$id");
	$email = $data[email];
	$groupid = $data[groupid];
	$subscription = $data[subscription];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX : Edit Email</title>
</head>

<body>
<font face=arial size=2>
<div align=center>
<form action=editemail.php method=post>
<input type=hidden name=id value="<?php print $id;?>">
<input type=hidden name=update value="Update">
<table border=0 bgcolor=#DCDCDC>
<tr><td colspan=2><font face=arial size=2><strong>Edit Email</strong></font></td>
<td><font face=arial size=2><strong>Variable</strong> <a href="javascript:openWindow('help.php?id=12')">?</a></font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Group:</font></td>
<td>
<select name="groupid">
<option value=0>-- Select --
<?php
	$sql = "select id, name from $tableGroup order by name";
	DropDownOptionsSelect($sql, $groupid);
?>
</select>
</td>
<td><font face=arial size=2>{GROUPNAME}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Email Address:</font></td>
<td><input name=email value="<?php print $email;?>" size=50 maxlength=100></td>
<td><font face=arial size=2>{EMAIL}</font></td>
</tr>
<tr><td align=right><font face=arial size=2>Subscription:</font></td>
<td>
        <input type="radio" name="subscription" value="1" <?php if ($subscription==1) print "checked";?>>
        <font size="2" face="Arial, Helvetica, sans-serif">Yes</font> 
        <input type="radio" name="subscription" value="0" <?php if ($subscription==0) print "checked";?>>
        <font size="2" face="Arial, Helvetica, sans-serif"> No </font>

</td>
<td><font face=arial size=2>&nbsp;</font></td>
</tr>
<tr><td colspan=3><hr size=1><font face=arial size=2><strong>Optional Personal Information</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Title:</font></td>
<td><input name=title value="<?php print $data[title];?>" size=10 maxlength=10>
<font face=arial size=2>(e.g. Mr. Miss. Mrs. Ms. Dr.)</font>
</td>
<td><font face=arial size=2>{TITLE}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Complete Name:</font></td>
<td><input name=name value="<?php print $data[name];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{NAME}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Address Line 1:</font></td>
<td><input name=address1 value="<?php print $data[address1];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{ADDRESS1}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Address Line 2:</font></td>
<td><input name=address2 value="<?php print $data[address2];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{ADDRESS2}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>City:</font></td>
<td><input name=city value="<?php print $data[city];?>" size=40 maxlength=50></td>
<td><font face=arial size=2>{CITY}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>State/Province:</font></td>
<td><input name=state value="<?php print $data[state];?>" size=40 maxlength=50></td>
<td><font face=arial size=2>{STATE} or {PROVINCE}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Zip/Postal Code:</font></td>
<td><input name=zip value="<?php print $data[zip];?>" size=20 maxlength=20></td>
<td><font face=arial size=2>{ZIP} or {POSTALCODE}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Country:</font></td>
<td><input name=country value="<?php print $data[country];?>" size=40 maxlength=60></td>
<td><font face=arial size=2>{COUNTRY}</font></td>
</tr>
<tr><td colspan=3><hr size=1><font face=arial size=2><strong>Optional Company Information</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Company/Organization Name:</font></td>
<td><input name=company value="<?php print $data[company];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{COMPANY}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Job Title:</font></td>
<td><input name=job value="<?php print $data[job];?>" size=40 maxlength=50></td>
<td><font face=arial size=2>{JOBTITLE}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Web URL:</font></td>
<td><input name=url value="<?php print $data[url];?>" size=50 maxlength=100></td>
<td><font face=arial size=2>{WEBURL}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Telephone:</font></td>
<td><input name=phone value="<?php print $data[phone];?>" size=40 maxlength=50></td>
<td><font face=arial size=2>{TELEPHONE}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Fax:</font></td>
<td><input name=fax value="<?php print $data[fax];?>" size=40 maxlength=50></td>
<td><font face=arial size=2>{FAX}</font></td>
</tr>
<tr><td colspan=3><hr size=1><font face=arial size=2><strong>Optional Additional Information</strong></font></td></tr>
<tr>
<td align=right><font face=arial size=2>Additional Field 1:</font></td>
<td><input name=field1 value="<?php print $data[field1];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{FIELD1}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Additional Field 2:</font></td>
<td><input name=field2 value="<?php print $data[field2];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{FIELD2}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Additional Field 3:</font></td>
<td><input name=field3 value="<?php print $data[field3];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{FIELD3}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Additional Field 4:</font></td>
<td><input name=field4 value="<?php print $data[field4];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{FIELD4}</font></td>
</tr>
<tr>
<td align=right><font face=arial size=2>Additional Field 5:</font></td>
<td><input name=field5 value="<?php print $data[field5];?>" size=40 maxlength=100></td>
<td><font face=arial size=2>{FIELD5}</font></td>
</tr>
<tr><td colspan=3><hr size=1></td></tr>
<tr>
<tr><td><font face=arial size=2>[<a href=deleteemail.php?id=<?php print $id;?> onclick="javascript:return confirm('Are you sure you want to delete it permanently?\n\n<?php print $email;?>');">delete</a>]</font></td><td colspan=2 align=right><input type="submit" name="update" value="Update"></td></tr>
</table>
</form>
</div>
<p>

<blockquote>
<font face=arial size=2>
<strong>Tips:</strong><br>
You can move email from one group to another by changing the 'Group' from drop down menu.<br>
Press 'Update' to make the change permanent.<br>
You can also remove the email address premanently by clicking 'delete'.<br>
Click browser back button or <u>Bulk Mailer X</u> link below to <strong>Cancel</strong>.
<p>
</blockquote>

</font>
<?php 
include "footer.php";
?>
</body>
</html>
<?php
}
?>
