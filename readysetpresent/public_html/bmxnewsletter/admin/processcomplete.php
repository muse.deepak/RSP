<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)

function process_complete($invalid, $duplicates, $added, $groupid, $log) {
	global $tableGroup, $tableMail, $tableLog;
	$log = addslashes($log);
	$totalrecords = $invalid + $duplicates + $added;
	$grouptotal = sql_data("select count(id) from $tableMail where groupid=$groupid");
	$grouptotal = $grouptotal[0];
	$groupname = sql_data("select name from $tableGroup where id=$groupid");
	$groupname = $groupname[0];
	$logid = sql_insert_id("insert into $tableLog (log, date, comment) values ('$log', sysdate(), 'Importing emails for group: <em>$groupname</em>')");

?>

<div align="center">
<h2>Processing Complete for Group<br><em><?php print $groupname; ?></em></h2>

<table border=0>
<tr bgcolor=#DCDCDC><td><font face=arial size=2>Total Records Processed:</font></td><td><font face=arial size=2><?php print $totalrecords; ?></td></tr>
<tr>
<td><font face=arial size=2>Invalid Records:</font></td><td><font face=arial size=2><?php print $invalid; ?></td></tr>
<tr><td><font face=arial size=2>Duplicate Records:</font></td><td><font face=arial size=2><?php print $duplicates; ?></td></tr>
<tr><td><font face=arial size=2 color=red>New Added Records:</font></td><td><font face=arial size=2 color=red><strong><?php print $added; ?></strong></td>
</tr>
<tr bgcolor=#DCDCDC><td colspan=2 align=center><font face=arial size=2><a href=checklog.php?id=<?php print $logid; ?>>Check Log</a><br><font size=1>(for invalid and duplicates)</font></font></td></tr>
<tr><td colspan=2><hr size=1></td></tr>
<tr bgcolor=#DCDCDC><td><font face=arial size=2 color=red>Group Total:</font></td><td><font face=arial size=2 color=red><strong><?php print $grouptotal; ?></strong></td>
</tr>
</table>
</div>
<p>
<?php
}
?>