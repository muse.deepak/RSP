<?php
// Bulk Mailer X (BMX)
// October, 2003. Release 3.
// Copyright (c) 1997-2002 Webx Networks (http://www.webx.net)
$id = $_GET[id];

switch ($id) {
########### Main Screen
    case 1:
        $topic = "Logs";
		$help = "Logs are generated whenever you enter/import email addresses. Logs are also generated when the mailer sends the newsletters out. These logs are just for information and debugging purpose. You can view the logs here or can just delete them when they are no longer needed.";
        break;
    case 2:
        $topic = "Import Emails";
		$help = "Here you can import email addresses from (a) another database table or (b) through uploading text file (one email address per line) or (c) enter individual/bulk email addresses on the web. After the processing of emails; logs are generated for invalid/duplicate emails. You can view the logs under Logs menu. New email addresses are always <strong>appended</strong> to the group you choose.";
        break;
    case 3:
        $topic = "Emails";
		$help = "You can search to find a particular email address. Useful utility to see if an email address is on the list. <p>With <strong>subscription</strong> you can subscribe/unsubscribe emails from a particular list/group. Newsletters are sent only to subscribed email addresses. By default new emails are subscribed.";
        break;
    case 4:
        $topic = "Newsletters";
		$help = "You can create newsletter at Add Newsletter, or view/edit.<p><strong>Bulk Mailer</strong> is where the real game happens. It is used to send a particular newsletter to a specific group/list. More details in Bulk Mailer screen.";
        break;
    case 5:
        $topic = "Groups";
		$help = "Here you can create group or list whatever you want to call it. You can also browse individual group and view/edit/delete email addresses on it.";
        break;

	    case 6:
        $topic = "Subscription";
		$help = "By default all email addresses are subscribed. However you can click on email address to edit it and unsubscribe. When an email address is unsubscribed an X appears on the subscription column.";
        break;

	    case 7:
        $topic = "Message Body";
		$help = "Here you can use both plain text or HTML. While using HTML you can use your favourite editor and use absolute URLs to references images. Images will automatically download from Internet when a user opens email. Absolute URL is that which starts with http:// e.g. http://www.yoursite.com/images/welcome.gif<p>When you set both text or HTML, the client end will display the appropriate one depending on its configuration. The mail will be sent using MIME, therefore it will be handled by the client email software automatically.<p>
		If you setup text only, the newsletter will be sent in text mode. If you setup HTML plus text, the newsletter will be sent in HTML. If you attach a file, the newsletter must be sent in HTML.";
        break;

	    case 8:
        $topic = "Batch Size";
		$help = "The batch size defines the number of newsletters sent to emails in 'one go'. This is particularly important if your PHP/web server is configured to time out after certain amount of time e.g. 50 seconds. You can set lower batch size to process less number of emails in one go. Or set higher size to process more in one go. See related note in <a href=help.php?id=9>Auto Mail</a>.";
        break;

	    case 9:
        $topic = "Auto Mail";
		$help = "If set to 'Yes' emails are processed of batch size after every 10 seconds. This way you can process large list with auto set to yes. The window will keep on refreshing showing you the progress after every 10 seconds. Once all emails are processed you will be notified on screen. Do not disturb the window.<p>With auto set to 'No' you will need to press 'Send' button for each batch. However you can also change each batch size.";
        break;

	    case 10:
        $topic = "Mailer ID";
		$help = "Just for information purpose and future releases. Each mail out is processed against a unique system generated id. This id is used to flag which emails have been processed (newsletter sent). A new id would mean a new newsletter (even if the same newsletter is selected). So please process one particular newsletter mail-out in one browser session.";
        break;

	    case 11:
        $topic = "Optional Headers";
		$help = "Optional headers are added to the email message while sending this particular newsletter. If the value is not set (it is not defined) it will not be added to the mail headers. Many of these headers are informational only. Mail reader client software use these headers, e.g. 'Priority' and/or 'Importance' if set to 'High' will show a mark of urgency in the mail reader.<p> You can set any header or leave all of them undefined.</p>";
        break;

	    case 12:
        $topic = "Variables";
		$help = "These variables can be used in newsletter body. They will be automatically replaced by the bulk mailer to appropriate value while sending messages. Using these variables you can customize individual messages.";
        break;

	    case 13:
        $topic = "Verification Process Messages";
		$help = "In these messages you can use two variables {GROUPNAME} and {EMAIL} only in the body of the messages. These variables will be replaced automatically by their values.";
        break;

	    case 14:
        $topic = "Verification Email Templates";
		$help = "Please do not disturb the verification URL defined in these templates. It uses the variable {BMXURL} where you have installed the public module of BMX. You can also use {GROUPNAME} to customize the message for a particular list/newsletter.";
        break;

	    case 15:
        $topic = "Attachment";
		$help = "You can attach a pdf, word, excel, or an image, whatever you can normally attach with an email. You just need to upload the file somewhere on the server where this application is running. When the newsletter goes out, the file will be attached automatically.";
        break;

	    case 16:
        $topic = "Filter Duplicate";
		$help = "If this option is selected, email addresses that already exist in the group will not be added again. If the email addresses you are importing are clean already without duplicates, turning this option off will increase the import speed significantly. Recommended to turn it off when you have large data to import without duplicates.";
        break;

	    case 17:
        $topic = "Overwrite Duplicate";
		$help = "For this option to work, 'Filter Duplicate' option must be active. What this does is overwrite an existing email data and update it with the values from the csv data. If 'Filter Duplicate' option is off, this will not work and new record will be added with same email address.";
        break;
		
	default: 
		$topic = "How to use Help?";
		$help = "Whenever you find a question mark <u>?</u> along with an item, you can press it to find the relevant help or information.";
		break;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>BMX Help : <?php print $topic; ?></title>
</head>

<body>
<font face=arial size=2>
<div align=right><a href=javascript:self.close()>close</a></div>
<br>
<table width=100% border=0><tr bgcolor="#DCDCDC"><td>
<font face=arial size=2>
<strong>BMX Help: <?php print $topic; ?></strong></td></tr>
<tr><td>
<font face=arial size=2>
<?php print $help; ?>
</td></tr></table>
<p>
<div align=center><a href=javascript:self.close()>close</a></div>
</font>
</body>
</html>
