<?php include "checksession.php"; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Bulk Mailer X</title>
</head>
<body>
<div align=center>
<font face=arial size=2>
<h2>Bulk Mailer X</h2>

<table border=0 width=95% align=center cellpadding="0" cellpadding="0">
<tr><td colspan=9 align=right bgcolor="#dafedc"><font face=arial size=2>
<a href="public.php">Public Subscription Module Guide</a>
| <a href="systemconfig.php">Configuration</a>
| <a href="passwd.php">Change Password</a>
| <a href="logout.php">Logout</a>
| <a href="javascript:openWindow('help.php')">Help</a></font></td></tr>
<tr>
<td bgcolor=#DCDCDC><font face=arial size=2>
<strong>Groups</strong> <a href="javascript:openWindow('help.php?id=5')">?</a>
</td><td rowspan="2" width="1" bgcolor="#DCDCDC"></td>
<td bgcolor=#DCDCDC><font face=arial size=2>
<strong>Newsletters</strong> <a href="javascript:openWindow('help.php?id=4')">?</a>
</td><td rowspan="2" width="1" bgcolor="DCDCDC"></td>
<td bgcolor=#DCDCDC><font face=arial size=2>
<strong>Emails</strong> <a href="javascript:openWindow('help.php?id=3')">?</a>
</td><td rowspan="2" width="1" bgcolor="DCDCDC"></td>
<td bgcolor=#DCDCDC><font face=arial size=2>
<strong>Import Emails</strong> <a href="javascript:openWindow('help.php?id=2')">?</a>
</td><td rowspan="2" width="1" bgcolor="DCDCDC"></td>
<td bgcolor=#DCDCDC><font face=arial size=2>
<strong>Logs</strong> <a href="javascript:openWindow('help.php?id=1')">?</a>
</td>
</tr>
<tr><td valign=top><font face=arial size=2>
<a href="newgroup.php">Create New</a><br>
<a href="groups.php">Browse/Edit</a><br>
<a href="exportcsv.php">Export csv</a><br>
<!-- <a href="merge.php">Merge</a><br> -->
</td>
<td valign=top><font face=arial size=2>
<a href="addnewsletter.php">Add Newsletter</a><br>
<a href="newsletters.php">View/Edit</a><br>
<font color=red>&gt;&gt;</font> <a href="mail.php">Bulk Mailer</a> <font color=red>&lt;&lt;</font>
</td>
<td valign=top><font face=arial size=2>
<a href="search.php">Search</a><br>
<a href="subscription.php">Subscription</a><br>
<a href="duplicates.php">Find Duplicates</a><br>
<a href="remove.php">Bulk Remove</a><br>
<a href="bulk-unsub.php">Bulk Unsubscribe</a><br>
</td>
<td valign=top>
<font face=arial size=2>
<a href="importfromdb.php">Database</a> (MySQL)<br>
<a href="importfromfile.php">File</a> (upload)<br>
<a href="importfromfile2.php">File</a> (on system path)<br>
<a href="importfromcsv.php">Member Data csv/text format</a><br>
<a href="importviashell.php">Import on Shell</a><br>
<a href="insert.php">Web</a> (bulk or individual)<br>
</ul>
</td>
<td valign=top>
<font face=arial size=2>
<a href="logs.php">Browse/Delete</a><br>
</ul>
</td>
</tr>
<tr><td colspan=9>
<font face=arial size=2>
<strong>Tip:</strong><br>
Click on <u>Bulk Mailer X - Main Page</u> (see below) on any page to reach this main page.<br>
<br>
<font color=red><strong>WARNING:</strong></font><br>
No SPAM. And please test your newsletter by sending it to your dummy list first. For example to see how it will look like in Yahoo Mail, Hotmail or other mail readers.
<p>
<font color=green><strong>SUPPORT:</strong></font><br>
Having problem? Contact <a href="mailto:support@webx.net?Subject=Bulk Mailer X">support@webx.net</a>.
<p>

</td></tr>
</table>
<?php include "footer.php"; ?>
</font>
</div>
</body>
</html>
