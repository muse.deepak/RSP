<?php
include_once("mysql.php");
include_once("functions.php");
include_once("analyticstracking.php");

$group = $_REQUEST[group]/1;
$email = trim($_REQUEST[email]);
$action = $_REQUEST[action];

$name = trim($_REQUEST[name]);
$city = trim($_REQUEST[city]);
$state = trim($_REQUEST[state]);
$country = trim($_REQUEST[country]);

$address1 = trim($_REQUEST[address1]);
$address2 = trim($_REQUEST[address2]);
$zip = trim($_REQUEST[zip]);
$phone = trim($_REQUEST[phone]);
$fax = trim($_REQUEST[fax]);
$title = trim($_REQUEST[title]);
$job = trim($_REQUEST[job]);
$url = trim($_REQUEST[url]);
$company = trim($_REQUEST[company]);
$field1 = trim($_REQUEST[field1]);
$field2 = trim($_REQUEST[field2]);
$field3 = trim($_REQUEST[field3]);
$field4 = trim($_REQUEST[field4]);
$field5 = trim($_REQUEST[field5]);


if ($action=="subscribe") {
	
	if ($group==0) {
		$popupMsg = $M_GROUP_INVALID;
		gotoError($redirectSubscribe);
	}

	if ($email=="") { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectSubscribe); 
	}
	
	else if (!check_email($email)) { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectSubscribe); 
	}
	
	else if (sql_exist("select email from $tableMail where email='$email' and groupid='$group' and subscription=1"))
	{
		$popupMsg = $M_ALREADY_SUBSCRIBED;
		gotoError($redirectSubscribe);
	}
	else {
		$pin = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
		
		$DATA = sql_data("select id,pin from $tableMail where email='$email' and groupid='$group' and subscription=0");
		if ($DATA[0]!="") {
			$emailid = $DATA[0];
			$pinold = $DATA[1];
			if ($pinold=="") {
				sql_query("update $tableMail set pin='$pin' where id='$emailid'");
			} else {
			$pin = $pinold;
			}
		} else {
			$sql = "insert into $tableMail (email, groupid, subscription, pin, name, city, state, country, address1,address2,zip,phone,fax,title,job,url,company,field1,field2,field3,field4,field5) values ('$email', $group,1,'$pin', '$name', '$city', '$state', '$country','$address1','$address2','$zip','$phone','$fax','$title','$job','$url','$company','$field1','$field2','$field3','$field4','$field5')";

			$emailid = sql_insert_id($sql);
		}
		//$popupMsg = $M_REQUEST_RECEIVED;
		//gotoVerify("subscribe",$redirectSuccess);

		$e = $emailid;
		$DATA = sql_data("select $tableMail.email, $tableGroup.name from $tableMail, $tableGroup where $tableMail.id='$e' and $tableGroup.id=$tableMail.groupid");
		$title = $M_SUBS_HEAD;
		$BODY = str_replace("{EMAIL}",$DATA[0],$M_SUBS_BODY);
		$BODY = str_replace("{GROUPNAME}",$DATA[1],$BODY);
		$popupMsg =  $BODY;
		gotoDone($verifyRedirect);
	}
}

if ($action=="unsubscribe") {

	if ($group==0) {
		$popupMsg = $M_GROUP_INVALID;
		gotoError($redirectUnSubscribe);
	}

	if ($email=="") { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectUnSubscribe); 
	}
	
	else if (!check_email($email)) { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectUnSubscribe); 
	}
	
	else if (!sql_exist("select email from $tableMail where email='$email' and groupid='$group' and subscription=1"))
	{
		$popupMsg = $M_NOT_SUBS;
		gotoError($redirectUnSubscribe);
	}
	else {
		$emailData = sql_data("select id, pin from $tableMail where email='$email' and groupid='$group'");
		$emailid = $emailData[id];
		$pin = $emailData[pin];
		if ($pin=="") {
		$pin = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
		sql_query("update $tableMail set pin='$pin' where id='$emailid'");
		}
		//$popupMsg = $M_REQUEST_UNSUB;
		//gotoVerify("unsubscribe",$redirectSuccess);

		$e=$emailid;
		$v=$pin;
		sql_query("update $tableMail set subscription=0 where id='$e' and pin='$v'");
		$DATA = sql_data("select $tableMail.email, $tableGroup.name from $tableMail, $tableGroup where $tableMail.id='$e' and $tableGroup.id=$tableMail.groupid");
		$title = $M_UNSUBS_HEAD;
		$BODY = str_replace("{EMAIL}",$DATA[0],$M_UNSUBS_BODY);
		$BODY = str_replace("{GROUPNAME}",$DATA[1],$BODY);
		$popupMsg = $BODY;
		gotoDone($verifyRedirect);

	}

}

?>