<?php

// ####################### gotoError ################

function gotoError($redirect="index.php") {
	global $popupMsg, $M_THANK_YOU_LINK;
?>
<html><body>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
  <!--
  window.alert("<?php print $popupMsg?>");
  location.href='<?php print $redirect;?>';
  //-->
  </SCRIPT>
<div align="center">
<font face="arial" size=2><?php print $popupMsg; ?></font><p>
<a href=<?php print $redirect;?>><?php print $M_THANK_YOU_LINK; ?></a></div>
</body></html>	
<?php
exit;
}

// ####################### gotoVerfiy ################

function gotoVerify($request,$redirect="index.php") {
	global $popupMsg, $emailid, $email, $pin, $M_THANK_YOU_LINK;
	global $tableGroup, $group, $BMXURL;
	global $E_VERIFY_SUBS, $E_VERIFY_SUBS_SUBJECT;
	global $E_VERIFY_UNSUBS, $E_VERIFY_UNSUBS_SUBJECT;

	if ($request=="subscribe") {
		$MAIL_MESSAGE = $E_VERIFY_SUBS;
		$SUBJECT = $E_VERIFY_SUBS_SUBJECT;
	}
	if ($request=="unsubscribe") {
		$MAIL_MESSAGE = $E_VERIFY_UNSUBS;
		$SUBJECT = $E_VERIFY_UNSUBS_SUBJECT;
	}

	$GROUP = sql_data("select name,from_name,from_email from $tableGroup where id=$group");

	$MAIL_MESSAGE = str_replace("{BMXURL}",$BMXURL,$MAIL_MESSAGE);
	$MAIL_MESSAGE = str_replace("{EMAILID}",$emailid,$MAIL_MESSAGE);
	$MAIL_MESSAGE = stripslashes(str_replace("{GROUPNAME}",$GROUP[name],$MAIL_MESSAGE));
	$MAIL_MESSAGE = str_replace("{PIN}",$pin,$MAIL_MESSAGE);
	$MAIL_MESSAGE = ereg_replace("(\r\n|\n|\r)", "\n",$MAIL_MESSAGE);

	$refresh = uniqid("");
	$MAIL_MESSAGE = str_replace("{REFRESH}",$refresh,$MAIL_MESSAGE);
	mail($email,$SUBJECT,$MAIL_MESSAGE,"From: $GROUP[from_name] <$GROUP[from_email]>");
?>
<html><body>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
  <!--
  window.alert("<?php print $popupMsg?>");
  location.href='<?php print $redirect;?>';
  //-->
  </SCRIPT>
<div align="center"><a href=<?php print $redirect;?>><?php print $M_THANK_YOU_LINK; ?></a></div>
</body></html>	
<?php
}

// ####################### gotoDone ################

function gotoDone($redirect="index.php") {
	global $popupMsg, $title, $M_THANK_YOU_LINK;
?>
<html><body>
<div align="center">
<font face=arial color="blue"><h3><?php print $title; ?></h3></font>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
  <!--
  window.alert("<?php print $popupMsg?>");
  location.href='<?php print $redirect;?>';
  //-->
  </SCRIPT>

<font face="arial" size=2><?php print $popupMsg; ?></font><p>
<a href=<?php print $redirect;?>><?php print $M_THANK_YOU_LINK; ?></a></div>
</body></html>	
<?php
exit;
}

// ####################### Check Email ################

function check_email($tmpEmail) {
	$sChars = "^[A-Za-z0-9\._-]+@([A-Za-z][A-Za-z0-9-]{1,62})(\.[A-Za-z][A-Za-z0-9-]{1,62})+$";
    if (!ereg("$sChars",$tmpEmail))
	{
         return(false);
	} 
	else  
	{ 
		return(true);
	}
}

// ####################### Javascript Validator ################

function javascriptValidator() {
	global $M_ENTER_VALID_EMAIL;
?>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
function validate()
{
	var validEmailMsg = '<?php print $M_ENTER_VALID_EMAIL;?>';
	if (document.newsletter.email.value=="") 
	{
		alert(validEmailMsg);
		document.newsletter.email.focus();
		return false;
	}
	if (document.newsletter.email.value.indexOf("@")==-1) 
	{
		alert(validEmailMsg);
		document.newsletter.email.focus();
		return false;
	}
	if (document.newsletter.email.value.indexOf(".")==-1) 
	{
		alert(validEmailMsg);
		document.newsletter.email.focus();
		return false;
	}
	if (document.newsletter.email.value.indexOf(" ")!=-1) 
	{
		alert(validEmailMsg);
		document.newsletter.email.focus();
		return false;
	}
	return true;
}
//-->
</SCRIPT>
<?php
}
?>