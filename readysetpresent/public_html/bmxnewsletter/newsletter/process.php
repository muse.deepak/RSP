<?php

foreach ($_POST as $key => $val) {
$_POST[$key] = preg_replace("/([^a-zA-Z0-9@.]+)/","",$val);
}

foreach ($_GET as $key => $val) {
$_GET[$key] = preg_replace("/([^a-zA-Z0-9@.]+)/","",$val);
}


include_once("mysql.php");
include_once("functions.php");
include_once("analyticstracking.php");

$group = $_REQUEST[group]/1;
$email = trim($_REQUEST[email]);
$action = $_REQUEST[action];

if ($action=="subscribe") {
	
	if ($group==0) {
		$popupMsg = $M_GROUP_INVALID;
		gotoError($redirectSubscribe);
	}

	if ($email=="") { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectSubscribe); 
	}
	
	else if (!check_email($email)) { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectSubscribe); 
	}
	
	else if (sql_exist("select email from $tableMail where email='$email' and groupid='$group' and subscription=1"))
	{
		$popupMsg = $M_ALREADY_SUBSCRIBED;
		gotoError($redirectSubscribe);
	}
	else {
		$pin = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
		
		$DATA = sql_data("select id,pin from $tableMail where email='$email' and groupid='$group' and subscription=0");
		if ($DATA[0]!="") {
			$emailid = $DATA[0];
			$pinold = $DATA[1];
			if ($pinold=="") {
				sql_query("update $tableMail set pin='$pin' where id='$emailid'");
			} else {
			$pin = $pinold;
			}
		} else {
			$sql = "insert into $tableMail (email, groupid, subscription, pin) values ('$email', $group,0,'$pin')";
			$emailid = sql_insert_id($sql);
		}
		$popupMsg = $M_REQUEST_RECEIVED;
		gotoVerify("subscribe",$redirectSuccess);
	}
}

if ($action=="unsubscribe") {

	if ($group==0) {
		$popupMsg = $M_GROUP_INVALID;
		gotoError($redirectUnSubscribe);
	}

	if ($email=="") { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectUnSubscribe); 
	}
	
	else if (!check_email($email)) { 
		$popupMsg = $M_EMAIL_INVALID; gotoError($redirectUnSubscribe); 
	}
	
	else if (!sql_exist("select email from $tableMail where email='$email' and groupid='$group' and subscription=1"))
	{
		$popupMsg = $M_NOT_SUBS;
		gotoError($redirectUnSubscribe);
	}
	else {
		$emailData = sql_data("select id, pin from $tableMail where email='$email' and groupid='$group'");
		$emailid = $emailData[id];
		$pin = $emailData[pin];
		if ($pin=="") {
		$pin = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
		sql_query("update $tableMail set pin='$pin' where id='$emailid'");
		}
		$popupMsg = $M_REQUEST_UNSUB;
		gotoVerify("unsubscribe",$redirectSuccess);
	}

}

?>
