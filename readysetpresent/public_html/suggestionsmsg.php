<?php

// First check to see if the for has been submitted
if (isset($_POST['submitted'])) {

$errors = array(); // Initialize error array.

// Check for a first name
if (empty($_POST['fname'])) {
$errors[] = 'You forgot to enter your First name.';
}

// Check for a last name
if (empty($_POST['lname'])) {
$errors[] = 'You forgot to enter your Last name.';
}

// Check for a I am a ...
if (empty($_POST['os0'])) {
$errors[] = 'You forgot to select who you are - I am ....';
}

// Check for a valid email address
if (!preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $_POST['email2'])) {
$errors[] = 'You need to enter a valid email address.';
}
if (!empty($_POST['human'])) {
$errors[] = 'Sorry cannot validate you.';
}
// Check for a message
if (empty($_POST['message'])) {
$errors[] = 'You forgot to enter a suggestion.';
}

if (empty($errors)) { // If everything is OK.

// Let's send an email
// Let's first send some email to the admin
$mailTo = "annatsiz@gmail.com";
$senderName = $_POST['fname'] + ' ' + $_POST['lname'];
$senderMail = $_POST['email2'];
$break .= "<br/>";
$eol .= "\r\n";
$sol .= "\n";
$headers .= 'To: '.$mailTo.' <'.$mailTo.'>'.$eol;
$headers .= 'From: '.$senderName.' <'.$senderMail.'>'.$eol;
$headers .= 'Date: '.date("r").$eol;
$headers .= 'Sender-IP: '.$_SERVER["REMOTE_ADDR"].$eol;
$headers .= 'X-Mailser: MCT Adv.PHP Mailer 1.0'.$eol;
$headers .= 'MIME-Version: 1.0'.$eol;
//$headers .= 'Content-Type: text/html; charset="windows-1251"\r\n';
$headers .= 'Content-Type: text/html; charset="iso-8859-1"'.$eol;
$subject = 'RSP Suggestion Form';

$msg .= '<font face=arial size=2>';
$msg .= '<strong>Suggestion:</strong> '.$_POST['message'].$break;
$msg .= 'You have recieved a message from RSP'.$break.$break;
$msg .= '<strong>First Name:</strong> '.$_POST['fname'].$break;
$msg .= '<strong>Last Name:</strong> '.$_POST['lname'].$break;
$msg .= '<strong>E-Mail:</strong> '.$_POST['email2'].$break;
$msg .= '<strong>I am a...:</strong> '.$_POST['os0'].$break;
$msg .= '<strong>Company/Institution:</strong> '.$_POST['company'].$break;
$msg .= '<strong>Location:</strong> '.$_POST['location'].$break;
$msg .= $break;
$msg .= '</font>';

// Mail it
mail($mailTo, $subject, $msg, $headers);

echo '<p if="mainhead">Thank you!</p>
<p class="textGreyBold">Thanks for contacting us. Someone will get back to you as soon as possible.</p>';

} else {
echo '<p id="mainhead">Error!</p>
<p class="textRed">The following error(s) occured.<br/>';
foreach ($errors as $errorMSG) { // Print each error.
echo " - $errorMSG<br/>\n";
} // End of Errors

echo 'Please go <a href="Javascript:history.go(-1)">back</a> and try again.</p>';

} // End of if (empty($errors)) IF Statement

} else { // Display the form.

echo'
<form method="post">
<table width="500" border="0" cellspacing="6" cellpadding="2" class="text">
        <tr>
          <td height="37" align="left"><label for="message">Suggestion *:</label></td>
          <td align="left"><textarea name="message" cols="45" rows="5" id="message">'.$_POST['$message'].'</textarea>
		   <input type="text" style="display:none; visibility:hidden" name="human" id="human" /></td>
        </tr>
        <tr>
          <td width="176" height="37" align="left"><label for="fname">First Name *: </label></td>
          <td width="298" align="left"><input class="contactBox" type="text" size="30" name="fname" id="fname" value="'.$_POST['fname'].'" /></td>
        </tr>
        <tr>
          <td height="37" align="left"><label for="lname">Last Name *:</label></td>
          <td align="left"><input class="contactBox" type="text" size="30" name="lname" id="lname" value="'.$_POST['lname'].'" /></td>
        </tr>
         <tr>
          <td height="37" align="left"><label for="email2">E-mail *:</label></td>
          <td align="left"><input class="contactBox" type="text" size="30" name="email2" id="email2" value="'.$_POST['email'].'" /></td>
        </tr>
         <tr>
          <td height="37" align="left"><label for="os0">I am a... *:</label></td>
          <td align="left">
         <select name="os0">
         <option value="0">--Select--</option>
  <option value="Student">Student</option>
  <option value="Instructor">Instructor</option>
  <option value="Manager">Manager</option>
  <option value="Other">Other</option>
  
</select></td>
        </tr>
         <tr>
          <td height="37" align="left"><label for="company">Company/Institution:</label></td>
          <td align="left"><input class="contactBox" type="text" size="30" name="company" id="company" value="'.$_POST['company'].'" /></td>
        </tr>
        <tr>
          <td height="37" align="left"><label for="location">Location:</label></td>
          <td align="left"><input class="contactBox" type="text" size="30" name="location" id="location" value="'.$_POST['location'].'" /></td>
        </tr>
       
        <tr>
          <td align="right">&nbsp;</td>
          <td align="center">&nbsp;</td>
        </tr>
        <tr>
          <td align="right"></td>
          <td align="center" ><input class="button" name="submit" type="submit" value="Submit" tabindex="5" /><input type="hidden" name="submitted" value="TRUE" /> &nbsp; <input class="button" name="reset" type="reset" value="Reset" /></td>
        </tr>
        <tr>
          <td colspan="2" align="center"></td>
        </tr>
      </table>
</p>';

} // End of submitted IF-ELSE statement

?> 
